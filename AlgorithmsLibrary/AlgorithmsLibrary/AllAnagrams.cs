﻿// http://www.testdome.com/Questions/Csharp/AllAnagrams/1308?testId=21&testDifficulty=Hard
// An anagram is a word formed from another by rearranging its letters, using all the original letters exactly once; 
// for example, orchestra can be rearranged into carthorse.
// Write a function which returns all anagrams of a given word (including the word itself) in any order.
// 
// For example GetAllAnagrams("abba") should return collection containing "aabb", "abab", "abba", "baab", "baba", "bbaa".

using System.Collections.Generic;

namespace AlgorithmsLibrary
{
  public static class AllAnagrams
  {
    //TODO: think, maybe there is possibility to create another implementation, something like GenerateParenthasesFast 
    public static ICollection<string> GetAllAnagrams(string str)
    {
      if (str.Length == 1)
        return new[] { str };

      var result = new HashSet<string>();
      char ch = str[0];

      foreach (string anagram in GetAllAnagrams(str.Substring(1)))
      {
        for (int i = 0; i <= anagram.Length; ++i)
        {
          string res = anagram.Insert(i, ch.ToString());
          result.Add(res);
        }
      }

      return result;
    }
  }
}