﻿// Implement an algorithm to print all valid 
// (i.e. properly opened and closed) 
// combinations of n-pairs of parentheses.

using System.Collections.Generic;
using System.Text;

namespace AlgorithmsLibrary
{
  public static class AllValidParenthesesCombinations
  {
    #region Fast

    public static List<string> GenerateParenthasesFast(int count)
    {
      var str = new char[count*2];
      var list = new List<string>();
      AddParen(list, count, count, str, 0);
      return list;
    }

    private static void AddParen(List<string> list, int leftRem, int rightRem, char[] str, int count)
    {
      if (leftRem < 0 || rightRem < leftRem) return;

      if (leftRem == 0 && rightRem == 0)
      {
        if (count == 0) return;

        var s = new string(str, 0, count);
        list.Add(s);
      }
      else
      {
        if (leftRem > 0)
        {
          str[count] = '(';
          AddParen(list, leftRem - 1, rightRem, str, count + 1);
        }

        if (rightRem > 0)
        {
          str[count] = ')';
          AddParen(list, leftRem, rightRem - 1, str, count + 1);
        }
      }
    }

    #endregion

    #region Simple

    public static ICollection<string> GenerateParenthasesSimple(int count)
    {
      if (count == 0)
        return new List<string>(0);

      if (count == 1)
        return new List<string> {"()"};

      var result = new HashSet<string>();

      foreach (string str in GenerateParenthasesSimple(count - 1))
      {
        for (int i = 0; i < str.Length; i++)
        {
          var sb = new StringBuilder(str);
          sb.Insert(i, "()");
          result.Add(sb.ToString());
        }
      }

      return result;
    }

    #endregion
  }
}