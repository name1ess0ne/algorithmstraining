﻿namespace TrainingProblems
{
    public class AreAnagrams
    {
        /// <summary>
        ///     http://www.testdome.com/Questions/Csharp/AreAnagrams/1291?testId=18&testDifficulty=Easy
        ///     An anagram is a word formed from another by rearranging its letters, using all the original letters exactly once;
        ///     for example, orchestra can be rearranged into carthorse. Write a function that checks if two words are each other's
        ///     anagrams.
        ///     For example, AreStringsAnagrams("momdad", "dadmom") should return true as arguments are anagrams.
        /// </summary>
        public static bool AreStringsAnagrams(string a, string b)
        {
            if (a.Length != b.Length)
                return false;

            var counts = new int[255];
            
            for (int i = 0; i < a.Length; i++)
            {
                counts[a[i]]++;
                counts[b[i]]--;
            }

            //var counts = new int['z' - 'a'];

            //for (int i = 0; i < a.Length; i++)
            //{
            //  counts[a[i] - 'a']++;
            //  counts[b[i] - 'a']--;
            //}

            for (int i = 0; i < counts.Length; i++)
            {
                if (counts[i] != 0)
                    return false;
            }

            return true;
        }

        public static bool AreStringsAnagramsForAscii(string a, string b)
        {
            if (a.Length != b.Length)
                return false;

            int result = 0;

            for (int i = 0; i < a.Length; i++)
            {
                result ^= (1 << (a[i] - 'a'));
                result ^= (1 << (b[i] - 'a'));
            }

            return result == 0;
        }

        public static bool IsInvertedStrings(string a, string b)
        {
            if (a.Length != b.Length)
                return false;

            int i = 0;
            int j = a.Length - 1;

            while (i < a.Length && j >= 0)
            {
                if (a[i] != b[j])
                    return false;

                ++i;
                --j;
            }

            return true;
        }
    }
}