﻿namespace AlgorithmsLibrary.BaseAlgorithms
{
  public static class BubbleSort
  {
    public static void Sort(int[] array)
    {
      for (int i = 0; i < array.Length - 1; ++i)
      {
        for (int j = i + 1; j < array.Length; ++j)
        {
          if (array[j] < array[i])
          {
            int tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
          }
        }
      }
    }
  }
}