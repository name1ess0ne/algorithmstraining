﻿using AlgorithmsLibrary.DataStructures;

namespace AlgorithmsLibrary.BaseAlgorithms
{

  //https://www.dropbox.com/s/0qpiuemz9seq470/shot_150521_102202.png?dl=0
  //https://begeeben.wordpress.com/2012/08/21/heap-sort-in-c/
  //http://www.sorting-algorithms.com/heap-sort
  //http://www.codeproject.com/Articles/79040/Sorting-Algorithms-Codes-in-C-NET
  public static class HeapSort
  {
    public static void Sort(int[] array)
    {
      //build max heap
      var maxHeap = new Heap<int>((x, y) => x.CompareTo(y), array);
      
      for (int i = array.Length - 1; i >= 0; --i)
      {
        array[i] = maxHeap.Pop();
      }
    }
  }
}