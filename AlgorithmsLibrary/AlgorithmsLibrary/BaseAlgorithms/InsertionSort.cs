﻿namespace AlgorithmsLibrary.BaseAlgorithms
{
  public static class InsertionSort
  {
    public static void Sort(int[] array)
    {
      for (var i = 1; i < array.Length; ++i)
      {
        Insert(array, i - 1, array[i]);
      }
    }

    private static void Insert(int[] array, int rightIndex, int value)
    {
      int j;
      for (j = rightIndex; j >= 0 && array[j] > value; j--)
      {
        array[j + 1] = array[j];
      }
      array[j + 1] = value;
    }
  }
}