﻿namespace AlgorithmsLibrary.BaseAlgorithms
{
  public static class MergeSort
  {
    public static void Sort(int[] array)
    {
      MergeSortRecursive(array, 0, array.Length - 1);
    }

    private static void MergeSortRecursive(int[] array, int left, int right)
    {
      if (right > left)
      {
        int mid = (right + left) / 2;

        MergeSortRecursive(array, left, mid);
        MergeSortRecursive(array, mid + 1, right);

        Merge(array, left, mid , right);
      }
    }

    private static void Merge(int[] array, int left, int mid, int right)
    {
      var lowHalf = new int[mid - left + 1];
      var highHalf = new int[right - mid];

      int k = left;
      int i;
      int j;

      for (i = 0; k <= mid; i++, k++)
      {
        lowHalf[i] = array[k];
      }

      for (j = 0; k <= right; j++, k++)
      {
        highHalf[j] = array[k];
      }

      k = left;
      i = 0;
      j = 0;

      // Repeatedly compare the lowest untaken element in
      // lowHalf with the lowest untaken element in highHalf
      // and copy the lower of the two back into array
      while ((i < lowHalf.Length) && (j < highHalf.Length))
      {
        if (lowHalf[i] < highHalf[j])
        {
          array[k] = lowHalf[i++];
        }
        else
        {
          array[k] = highHalf[j++];
        }
        k++;
      }

      // Once one of lowHalf and highHalf has been fully copied
      // back into array, copy the remaining elements from the
      // other temporary array back into the array
      while (i < lowHalf.Length)
      {
        array[k++] = lowHalf[i++];
      }

      while (j < highHalf.Length)
      {
        array[k++] = highHalf[j++];
      }
    }
  }
}