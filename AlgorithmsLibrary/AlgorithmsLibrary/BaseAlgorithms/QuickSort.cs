﻿namespace AlgorithmsLibrary.BaseAlgorithms
{
  public static class QuickSort
  {
    public static void Sort(int[] array)
    {
      QuickSortRecursive(array, 0, array.Length - 1);
    }

    private static int Partition(int[] array, int p, int r)
    {
      // Compare array[j] with array[r], for j = p, p+1,...r-1
      // maintaining that:
      //  array[p..q-1] are values known to be <= to array[r]
      //  array[q..j-1] are values known to be > array[r]
      //  array[j..r-1] haven't been compared with array[r]
      // If array[j] > array[r], just increment j.
      // If array[j] <= array[r], swap array[j] with array[q],
      //   increment q, and increment j. 
      // Once all elements in array[p..r-1]
      //  have been compared with array[r],
      //  swap array[r] with array[q], and return q.

      var q = p;

      for (var j = p; j < r; j++)
      {
        if (array[j] <= array[r])
        {
          Swap(array, j, q);
          q++;
        }
      }

      Swap(array, r, q);
      return q;
    }

    private static void QuickSortRecursive(int[] array, int p, int r)
    {
      if (p < r)
      {
        int pivot = Partition(array, p, r);
        QuickSortRecursive(array, p, pivot - 1);
        QuickSortRecursive(array, pivot + 1, r);
      }
    }

    private static void Swap(int[] array, int firstIndex, int secondIndex)
    {
      var temp = array[firstIndex];
      array[firstIndex] = array[secondIndex];
      array[secondIndex] = temp;
    }
  }
}