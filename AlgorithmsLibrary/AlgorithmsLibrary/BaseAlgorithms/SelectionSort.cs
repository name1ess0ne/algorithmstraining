﻿// 1. Find the smallest card. Swap it with the first card.
// 2. Find the second-smallest card. Swap it with the second card.
// 3. Find the third-smallest card. Swap it with the third card.
// 4. Repeat finding the next-smallest card, and swapping it into the correct position until the array is sorted.

namespace AlgorithmsLibrary.BaseAlgorithms
{
  public static class SelectionSort
  {
    public static void SortInlined(int[] array)
    {
      for (var i = 0; i < array.Length; ++i)//Length - 1?
      {
        int minValue = array[i];
        int minIndex = i;

        for (int j = minIndex + 1; j < array.Length; ++j)
        {
          if (array[j] < minValue)
          {
            minIndex = j;
            minValue = array[j];
          }
        }

        int temp = array[i];
        array[i] = array[minIndex];
        array[minIndex] = temp;
      }
    }

    //----------------------------------------------

    private static int IndexOfMinimum(int[] array, int startIndex)
    {
      int minValue = array[startIndex];
      int minIndex = startIndex;

      for (int i = minIndex + 1; i < array.Length; i++)
      {
        if (array[i] < minValue)
        {
          minIndex = i;
          minValue = array[i];
        }
      }

      return minIndex;
    }

    private static void Swap(int[] array, int firstIndex, int secondIndex)
    {
      int temp = array[firstIndex];
      array[firstIndex] = array[secondIndex];
      array[secondIndex] = temp;
    }

    public static void Sort(int[] array)
    {
      for (int i = 0; i < array.Length; ++i)
      {
        int minIndex = IndexOfMinimum(array, i);
        Swap(array, i, minIndex);
      }
    }
  }
}