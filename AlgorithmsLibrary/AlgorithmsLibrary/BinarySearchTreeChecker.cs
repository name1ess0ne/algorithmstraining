﻿// http://www.testdome.com/Questions/Csharp/BinarySearchTree/838?testId=21&testDifficulty=Hard
// Write a function that checks if a given binary tree is a valid binary search tree. A binary search tree (BST) 
// is a binary tree where the value of each node is larger or equal to the values in all the nodes in that node's 
// left subtree and is smaller than the values in all the nodes in that node's right subtree.
// 
// For example, for the following tree
// - n1 (Value: 1, Left: null, Right: null)
// - n2 (Value: 2, Left: n1, Right: n3)
// - n3 (Value: 3, Left: null, Right: null)
// call to IsValidBST(n2) should return true since a tree with root at n2 is a valid binary search tree. 
// Explanation: Subtrees rooted at nodes n1 and n3 are valid binary search trees as they have no children. 
// A tree rooted at node n2 is a valid binary search tree since its value (2) is larger or equal to 
// the largest value in its left subtree (1, rooted at n1) and is smaller than the smallest value in its right subtree (3 rooted at n3).

using System;
using System.Collections.Generic;
using AlgorithmsLibrary.DataStructures;

namespace AlgorithmsLibrary
{
  public static class BinarySearchTreeChecker
  {
    public static bool IsValidBST(Node root)
    {
      return IsBST(root);
    }

    private static bool IsBST(Node node, int minData = int.MinValue, int maxData = int.MaxValue)
    {
      // для дерева из 3-х элементов получился такой call stack проверок
      //
      // int.MinValue < root <= int.MaxValue
      // int.MinValue < left <= root
      // root < right <= int.MaxValue
      //
      // т.е. left <= root < right, что соответствует условию задачи!

      if (node == null)
        return true;

      if (minData < node.Value && node.Value <= maxData)
        return IsBST(node.Left, minData, node.Value) && IsBST(node.Right, node.Value, maxData);
      
      return false;
    }

    //NOTE: просто пример как то же самое можно записать через стек
    public static bool IsValidBSTStack(Node root)
    {
      if (root == null) 
        return true;

      var stack = new Stack<Tuple<Node, int, int>>();
      stack.Push(new Tuple<Node, int, int>(root, int.MinValue, int.MaxValue));

      while (stack.Count > 0)
      {
        Tuple<Node, int, int> tuple = stack.Pop();

        Node node = tuple.Item1;
        int minData = tuple.Item2;
        int maxData = tuple.Item3;

        if (node.Value <= minData || node.Value > maxData)
          return false;

        if(node.Left != null)
          stack.Push(new Tuple<Node, int, int>(node.Left, minData, node.Value));

        if (node.Right != null)
          stack.Push(new Tuple<Node, int, int>(node.Right, node.Value, maxData));
      }

      return true;
    }
  }
}