﻿using AlgorithmsLibrary.DataStructures;

namespace AlgorithmsLibrary
{
    public static class BinaryTreeCommonAncestor
    {
        #region Solution 1

        public static Node FindCommonAncestor(Node root, Node node1, Node node2)
        {
            bool isNodesInTree = IsExist(root, node1) && IsExist(root, node2);

            if (!isNodesInTree)
                return null;

            return FindCommonAncestorInternal(root, node1, node2);
        }

        private static Node FindCommonAncestorInternal(Node root, Node node1, Node node2)
        {
            if (root == null) return null;

            // We checked above that all node are in the tree, and we traversing from top to bottom. 
            // If found some node, that means that node is common ancestor.
            if (root == node1 || root == node2) return root;

            bool node1OnTheLeft = IsExist(root.Left, node1);
            bool node2OnTheLeft = IsExist(root.Left, node2);

            if (node1OnTheLeft != node2OnTheLeft)
                return root;

            Node nextRoot = node1OnTheLeft ? root.Left : root.Right;
            return FindCommonAncestor(nextRoot, node1, node2);
        }

        private static bool IsExist(Node root, Node searchNode)
        {
            if (root == null) return false;
            if (root == searchNode) return true;

            return IsExist(root.Left, searchNode) || IsExist(root.Right, searchNode);
        }

        #endregion

        #region Solution 2

        private struct Result
        {
            public static readonly Result Empty = new Result();

            public readonly Node Node;
            public readonly bool IsAncestor;

            public Result(Node node, bool isAncestor)
            {
                Node = node;
                IsAncestor = isAncestor;
            }
        }

        public static Node FindCommonAncestorOptimized(Node root, Node node1, Node node2)
        {
            Result result = FindCommonAncestorOptimizedInternal(root, node1, node2);
            
            return result.IsAncestor ? result.Node : null;
        }

        private static Result FindCommonAncestorOptimizedInternal(Node root, Node node1, Node node2)
        {
            if (root == null) return Result.Empty;

            if (root == node1 && root == node2)
                return new Result(root, true);

            Result rightResult = FindCommonAncestorOptimizedInternal(root.Right, node1, node2);
            if (rightResult.IsAncestor) // Found common ancestor
                return rightResult;

            Result leftResult = FindCommonAncestorOptimizedInternal(root.Left, node1, node2);
            if (leftResult.IsAncestor) // Found common ancestor
                return leftResult;

            if (rightResult.Node != null && leftResult.Node != null)
                return new Result(root, true); // This is the common ancestor

            if (root == node1 || root == node2)
            {
                // If we're currently at node1 or node2, and we also found one of
                // those nodes in a subtree, then this is truly an ancestor
                // and the flag should be true. 
                bool isAncestor = rightResult.Node != null || leftResult.Node != null;

                return new Result(root, isAncestor);
            }

            return new Result(rightResult.Node ?? leftResult.Node, false);
        }

        #endregion
    }
}