﻿// Найти в бинарном дереве level с наибольшим количеством элементов.
// Вернуть level (int) и количество элементов (int).

using System;
using System.Collections.Generic;
using AlgorithmsLibrary.DataStructures;

namespace AlgorithmsLibrary
{
  public static class BinaryTreeLargestLevel
  {
    public static Tuple<int, int> FindLargestLevel(Node root)
    {
      var queue = new Queue<Node>();
      queue.Enqueue(root);

      int level = 0;
      int itemsOnCurrentLevel = 1;

      int maxLevel = level;
      int maxCount = itemsOnCurrentLevel;

      while (queue.Count > 0)
      {
        Node item = queue.Dequeue();
        itemsOnCurrentLevel--;

        if (item.Left != null)
          queue.Enqueue(item.Left);

        if (item.Right != null)
          queue.Enqueue(item.Right);

        if (itemsOnCurrentLevel == 0)
        {
          level++;
          itemsOnCurrentLevel = queue.Count;

          if (maxCount < queue.Count)
          {
            maxCount = queue.Count;
            maxLevel = level;
          }
        }
      }

      return new Tuple<int, int>(maxLevel, maxCount);
    }
  }
}