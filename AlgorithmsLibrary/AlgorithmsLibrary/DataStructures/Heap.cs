﻿//http://en.wikipedia.org/wiki/Heap_(data_structure)

using System;
using System.Collections;
using System.Collections.Generic;

namespace AlgorithmsLibrary.DataStructures
{
  public class Heap<T> : IEnumerable<T>
  {
    private readonly Comparison<T> _comparer;
    private readonly List<T> _heap;

    public int Count
    {
      get { return _heap.Count; }
    }

    #region Constructors

    public Heap(Comparison<T> comparer)
    {
      _comparer = comparer;
      _heap = new List<T>();
    }

    public Heap(Comparison<T> comparer, int capacity)
    {
      _comparer = comparer;
      _heap = new List<T>(capacity);
    }

    public Heap(Comparison<T> comparer, IEnumerable<T> items)
    {
      _comparer = comparer;

      // O(n log n)
      //_heap = new List<T>();

      //foreach (T item in items)
      //{
      //  Push(item);
      //}

      // O(n)
      _heap = new List<T>(items);
      BuildHeap(_heap);
    }

    #endregion

    #region IEnumerable<T> Members

    public IEnumerator<T> GetEnumerator()
    {
      return _heap.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    #endregion

    //find-max or find-min: find the maximum item of a max-heap or a minimum item of a min-heap (a.k.a. peek)
    public T Peek()
    {
      if (_heap.Count == 0)
        return default(T);

      return _heap[0];
    }

    //insert: adding a new key to the heap (a.k.a., push[1])
    public void Push(T value)
    {
      _heap.Add(value);
      ShiftUp(_heap.Count - 1);
    }

    //extract-min [or extract-max]: returns the node of minimum value from a min heap [or maximum value from a max heap] after removing it from the heap (a.k.a., pop)
    public T Pop()
    {
      T result = Peek();
      RemoveMax();
      return result;
    }

    //delete-max or delete-min: removing the root node of a max- or min-heap, respectively
    public void RemoveMax()
    {
      Swap(_heap, 0, _heap.Count - 1);
      _heap.RemoveAt(_heap.Count - 1);
      ShiftDown(0);
    }

    //replace: pop root and push a new key. More efficient than pop followed by push, since only need to balance once, not twice, and appropriate for fixed-size heaps.
    public void Replace(T value)
    {
      if (_heap.Count > 0)
      {
        _heap[0] = value;
        ShiftDown(0);
      }
      else
      {
        _heap.Add(value);
      }
    }

    //heapify: create a heap out of given array of elements
    private void BuildHeap(IList<T> array)
    {
      int start = (array.Count / 2) - 1;

      for (int counter = start; counter >= 0; counter--)
      {
        ShiftDown(counter);
      }
    }

    private static void Swap(IList<T> array, int index1, int index2)
    {
      T tmp = array[index1];
      array[index1] = array[index2];
      array[index2] = tmp;
    }

    //sift-up: move a node up in the tree, as long as needed; used to restore heap condition after insertion. Called "sift" because node moves up the tree until it reaches the correct level, as in a sieve.
    private void ShiftUp(int index)
    {
      while (true)
      {
        int parentIndex = GetParentIndex(index);

        T item = _heap[index];
        T parent = _heap[parentIndex];

        if (_comparer(parent, item) >= 0)
          return;

        Swap(_heap, parentIndex, index);
        index = parentIndex;
      }
    }

    //sift-down: move a node down in the tree, similar to sift-up; used to restore heap condition after deletion or replacement.
    private void ShiftDown(int index)
    {
      while (true)
      {
        int left = GetLeftChildIndex(index);
        int right = GetRightChildIndex(index);
        int largest;

        if (left < _heap.Count && _comparer(_heap[left], _heap[index]) > 0)
        {
          largest = left;
        }
        else
        {
          largest = index;
        }

        if (right < _heap.Count && _comparer(_heap[right], _heap[largest]) > 0)
        {
          largest = right;
        }

        if (largest != index)
        {
          Swap(_heap, index, largest);
          index = largest;
          continue;
        }
        break;
      }
    }

    private static int GetParentIndex(int index)
    {
      return (index - 1) / 2;
    }

    private static int GetLeftChildIndex(int index)
    {
      return 2 * index + 1;
    }

    private static int GetRightChildIndex(int index)
    {
      return 2 * index + 2;
    }
  }
}