﻿using System;

namespace AlgorithmsLibrary
{
  public static class ExcelColumnNumberConverter
  {
    public static string NumberToString(int number)
    {
      string columnName = string.Empty;

      int dividend = number;

      while (dividend > 0)
      {
        int modulo = (dividend - 1) % 26;
        columnName = Convert.ToChar(65 + modulo) + columnName;
        dividend = (dividend - modulo) / 26;
      }

      return columnName;
    }

    public static string ColumnAddress(int col)
    {
      if (col <= 26)
      {
        return Convert.ToChar(col + 64).ToString();
      }

      int div = col / 26;
      int mod = col % 26;

      if (mod == 0)
      {
        mod = 26;
        div--;
      }

      return ColumnAddress(div) + ColumnAddress(mod);
    }

    public static int ColumnNumber(string colAdress)
    {
      var digits = new int[colAdress.Length];

      for (int i = 0; i < colAdress.Length; ++i)
      {
        digits[i] = Convert.ToInt32(colAdress[i]) - 64;
      }

      int mul = 1;
      int res = 0;

      for (int pos = digits.Length - 1; pos >= 0; --pos)
      {
        res += digits[pos] * mul;
        mul *= 26;
      }

      return res;
    }
  }
}