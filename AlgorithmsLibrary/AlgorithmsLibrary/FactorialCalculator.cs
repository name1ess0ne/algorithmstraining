﻿using System.Collections.Generic;

namespace AlgorithmsLibrary
{
  public static class FactorialCalculator
  {
    public static int Factorial(int n)
    {
      if (n == 0)
        return 1;

      return n * Factorial(n - 1);
    }

    public static int FactorialStack(int n)
    {
      var stack = new Stack<int>();
      stack.Push(n);

      int result = 1;

      while (stack.Count > 0)
      {
        int num = stack.Pop();

        if (num == 0)
          break;

        result = result * num;
        stack.Push(num - 1);
      }

      return result;
    }
  }
}
