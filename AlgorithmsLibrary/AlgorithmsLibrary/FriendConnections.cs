﻿// http://www.testdome.com/Questions/Csharp/Friends/1295?testId=21&testDifficulty=Hard
// Given a data structure representing a social network, write a function that finds friends of a certain degree. 
// Friends of the first degree are a member's immediate friends, friends of the second degree are 
// friends of a member's friends excluding first degree friends, etc.
// 
// For example, if A is a friend with B and B is a friend with C, then GetFriendsOfDegree(A, 2) 
// should return C since C is the only second degree friend of A (B is a first degree friend of A).

using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsLibrary
{
    public class Member
    {
        public Member(string email)
            : this(email, new List<Member>())
        {
        }

        public Member(string email, ICollection<Member> friends)
        {
            Email = email;
            Friends = friends;
        }

        public string Email { get; private set; }

        public ICollection<Member> Friends { get; private set; }

        public void AddFriends(ICollection<Member> friends)
        {
            foreach (Member friend in friends)
                Friends.Add(friend);
        }

        public void AddFriend(Member friend)
        {
            Friends.Add(friend);
        }
    }

    public class MemberInfo
    {
        public MemberInfo(Member member, int level)
        {
            Member = member;
            Level = level;
        }

        public Member Member { get; private set; }
        public int Level { get; private set; }
    }

    public static class FriendConnections
    {
        public static List<Member> GetFriendsOfDegreeQueue(Member member, int degree)
        {
            var result = new List<Member>();
            var processedFriends = new HashSet<Member>();
            var membersToProcess = new Queue<MemberInfo>();

            membersToProcess.Enqueue(new MemberInfo(member, 0));
            processedFriends.Add(member);

            while (membersToProcess.Count > 0)
            {
                MemberInfo memberInfo = membersToProcess.Dequeue();

                if (memberInfo.Level == degree)
                {
                    result.Add(memberInfo.Member);
                }
                else if (memberInfo.Level > degree)
                {
                    break;
                }
                else
                {
                    foreach (Member friend in memberInfo.Member.Friends)
                    {
                        if (!processedFriends.Contains(friend))
                        {
                            membersToProcess.Enqueue(new MemberInfo(friend, memberInfo.Level + 1));
                            processedFriends.Add(friend);
                        }
                    }
                }
            }

            return result;
        }

        public static List<Member> GetFriendsOfDegreePureQueue(Member member, int degree)
        {
            var processedFriends = new HashSet<Member>();
            var membersToProcess = new Queue<Member>();

            membersToProcess.Enqueue(member);
            processedFriends.Add(member);

            int level = 0;
            int friendsOnLevelCounter = membersToProcess.Count;

            while (membersToProcess.Count > 0 && level < degree)
            {
                Member item = membersToProcess.Dequeue();
                friendsOnLevelCounter--;

                foreach (Member friend in item.Friends)
                {
                    if (!processedFriends.Contains(friend))
                    {
                        membersToProcess.Enqueue(friend);
                        processedFriends.Add(friend);
                    }
                }

                if (friendsOnLevelCounter == 0)//we reach new level
                {
                    level++;
                    friendsOnLevelCounter = membersToProcess.Count;
                }
            }

            return membersToProcess.ToList();
        }

        public static List<Member> GetFriendsOfDegreeTwoList(Member member, int degree)
        {
            var processedFriends = new HashSet<Member>();
            var currentLevel = new List<Member>();
            var nextLevel = new List<Member>();

            currentLevel.Add(member);
            processedFriends.Add(member);
            int level = 0;

            while (currentLevel.Count > 0 && level < degree)
            {
                nextLevel.Clear();
                foreach (Member m in currentLevel)
                {
                    foreach (Member friend in m.Friends)
                    {
                        if (!processedFriends.Contains(friend))
                        {
                            processedFriends.Add(friend);
                            nextLevel.Add(friend);
                        }
                    }
                }

                List<Member> tmp = currentLevel;
                currentLevel = nextLevel;
                nextLevel = tmp;
                level++;
            }

            return currentLevel;
        }

        public static List<Member> GetFriendsOfDegreeTwoQueue(Member member, int degree)
        {
            var processedFriends = new HashSet<Member>();
            var currentLevel = new Queue<Member>();
            var nextLevel = new Queue<Member>();

            currentLevel.Enqueue(member);
            processedFriends.Add(member);
            int level = 0;

            while (currentLevel.Count > 0 && level < degree)
            {
                Member m = currentLevel.Dequeue();

                foreach (Member friend in m.Friends)
                {
                    if (!processedFriends.Contains(friend))
                    {
                        processedFriends.Add(friend);
                        nextLevel.Enqueue(friend);
                    }
                }

                if (currentLevel.Count == 0)
                {
                    Queue<Member> tmp = currentLevel;
                    currentLevel = nextLevel;
                    nextLevel = tmp;
                    level++;
                }
            }

            return currentLevel.ToList();
        }
    }
}