﻿// http://www.testdome.com/Questions/Csharp/Frog/660?testId=21&testDifficulty=Easy
// A frog only moves forward, but it can move in steps 1 inch long or in jumps 2 inches long. 
// A frog can cover the same distance using different combinations of steps and jumps. 
// Write a function that calculates the number of different combinations a frog can use to cover a given distance.
//
// For example, a distance of 3 inches can be covered in three ways: step-step-step, step-jump, and jump-step.

using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsLibrary
{
  public class Frog
  {
    private static readonly Dictionary<int, int> NumberOfWaysCache = new Dictionary<int, int>();

    public static int NumberOfWays(int n)
    {
      if (n == 2) return 2;
      if (n == 1) return 1;
      if (n == 0) return 0;

      int result;
      if (NumberOfWaysCache.TryGetValue(n, out result))
      {
        return result;
      }

      result = NumberOfWays(n - 1) + NumberOfWays(n - 2);

      NumberOfWaysCache[n] = result;

      return result;
    }

    //NOTE: but without cache it is too slow
    //TODO: think how to add caching here, is it possible?
    public static int NumberOfWaysStack(int n)
    {
      int result = 0;
      var stack = new Stack<int>();
      stack.Push(n);

      while (stack.Any())
      {
        int num = stack.Pop();

        if (num == 2)
        {
          result += 2;
          continue;
        }

        if (num == 1)
        {
          result += 1;
          continue;
        }

        if (num == 0)
        {
          result += 0;
          continue;
        }

        stack.Push(num - 1);
        stack.Push(num - 2);
      }

      return result;
    }
  }
}