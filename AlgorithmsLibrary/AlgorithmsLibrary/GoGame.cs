﻿// https://www.dropbox.com/s/fn5hidmcwpj1siw/shot_150402_141844.png?dl=0
// You are creating a Go-game, which is played by two players. Players alternate turns by placing stones (black[O] and white[#]) on a 
// board. A group of like color stones is captured and removed from the board when surrounded by unlike color stones. 
// For example, if a black stone were to be placed at D3 in Figure 2, both white stones would be captured because they have no more 
// liberties.  

// A rule of Japanese Go states, any placement of a stone which would result in "suicide" is illegal. In other words, a stone may not be 
// placed on the board where it will have no liberties as a result 

// For example, this rule enforces that white may not place a stone at 
// C3 in Figure 1 because it would have no liberty 
// D3 in Figure 2 because the chain of adjacent white stone would no longer have a liberty 

// Figure 1          Figure 2 
// 5 - - - - -       5 - - - - - -
//   | | | | |         | | | | | |
// 4 - - O - -       4 - - O O - -
//   | | | | |         | | | | | |
// 3 - O - O -       3 - O # - O -
//   | | | | |         | | | | | |
// 2 - - O - -       2 - O # O - -
//   | | | | |         | | | | | |
// 1 - - - - -       1 - - O - - -
//   A B C D E         A B C D E F

namespace AlgorithmsLibrary
{
    public static class GoGame
    {
        /// <summary>
        /// Indicates whether a stone placement is legal.
        /// </summary>
        /// <param name="isBIackStone">True when stone placement is black. Otherwise, false.</param>
        /// <param name="row">X-coordinate (index) of the stone being placed.</param>
        /// <param name="col">Y-coordinate (index) of the stone being placed.</param>
        /// <param name="blackStones">Current state of black stones on the board.</param>
        /// <param name="whiteStones">Current state of white stones on the board.</param>
        /// <returns>True if the stone placement is legal based on the current state of the board.</returns>
        public static bool IsLegalMoveAlternative(bool isBIackStone, int row, int col, bool[,] blackStones, bool[,] whiteStones)
        {
            bool[,] ally = isBIackStone ? blackStones : whiteStones;
            bool[,] enemy = isBIackStone ? whiteStones : blackStones;

            var visited = new bool[ally.GetLength(0), ally.GetLength(1)];
            //ally[row, col] = true; // if we do that, we do not need isStart parameter, but source array will be changed

            return IsLegalMoveX(row, col, ally, enemy, visited, true);
        }

        private static bool IsLegalMoveX(int row, int col, bool[,] ally, bool[,] enemy, bool[,] visited, bool isStart = false)
        {
            if (row < 0 || row >= ally.GetLength(0))
                return false;

            if (col < 0 || col >= ally.GetLength(1))
                return false;

            //there is enemy
            if (enemy[row, col])
                return false;

            if (!isStart)//do not check the main case for the start position
            {
                if (visited[row, col])
                    return false;

                //there is no enemy and no ally
                if (!ally[row, col])
                    return true;//we found the exit, it is a free cell!                
            }

            visited[row, col] = true;

            bool result = IsLegalMoveX(row - 1, col, ally, enemy, visited) ||
                          IsLegalMoveX(row + 1, col, ally, enemy, visited) ||
                          IsLegalMoveX(row, col - 1, ally, enemy, visited) ||
                          IsLegalMoveX(row, col + 1, ally, enemy, visited);

            return result;
        }

        //------------------------

        /// <summary>
        /// Indicates whether a stone placement is legal.
        /// </summary>
        /// <param name="isBIackStone">True when stone placement is black. Otherwise, false.</param>
        /// <param name="row">X-coordinate (index) of the stone being placed.</param>
        /// <param name="col">Y-coordinate (index) of the stone being placed.</param>
        /// <param name="blackStones">Current state of black stones on the board.</param>
        /// <param name="whiteStones">Current state of white stones on the board.</param>
        /// <returns>True if the stone placement is legal based on the current state of the board.</returns>
        public static bool IsLegalMove(bool isBIackStone, int row, int col, bool[,] blackStones, bool[,] whiteStones)
        {
            bool[,] ally = isBIackStone ? blackStones : whiteStones;
            bool[,] enemy = isBIackStone ? whiteStones : blackStones;

            // we cannot turn on the occupied positions
            if (ally[row, col] || enemy[row, col])
                return false;

            int rows = ally.GetLength(0);
            int cols = ally.GetLength(1);

            return IsLegalMove(row, col, ally, enemy, new bool[rows, cols]);
        }

        private static bool IsLegalMove(int row, int col, bool[,] ally, bool[,] enemy, bool[,] visited)
        {
            visited[row, col] = true;

            bool result = CanMove(row - 1, col, ally, enemy, visited) || //can move up
                          CanMove(row, col - 1, ally, enemy, visited) || //can move left
                          CanMove(row + 1, col, ally, enemy, visited) || //can move down
                          CanMove(row, col + 1, ally, enemy, visited);   //can move right

            return result;
        }

        private static bool CanMove(int row, int col, bool[,] ally, bool[,] enemy, bool[,] visited)
        {
            //check bounds
            if (row < 0 || row >= ally.GetLength(0) || col < 0 || col >= ally.GetLength(1))
                return false;

            //we have been here before, ignore
            if (visited[row, col])
                return false;

            //there is enemy
            if (enemy[row, col])
                return false;

            //there is no enemy and no ally
            if (!ally[row, col])
                return true;//we found the exit, it is a free cell!

            //there is no enemy but ally, start recursion
            return IsLegalMove(row, col, ally, enemy, visited);
        }
    }
}