﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace AlgorithmsLibrary
{
  public static class GridPathFinder
  {
    /// <summary>
    /// Есть grid n*n в нем есть 0 и 1. 
    /// Можно путешествовать только по 1. 
    /// Перемещения по диагонали запрещены. 
    /// Найти максимальный непрерывный путь, 
    /// который проходит по 1 и не пересекается сам с собой. 
    /// Путь может начинаться в любой клетке.
    /// </summary>
    public static int GetLongestPath(int[,] grid)
    {
      int rows = grid.GetUpperBound(0);
      int cols = grid.GetUpperBound(1);

      int maxPathLength = 0;

      for (int row = 0; row < rows; ++row)
      {
        for (int col = 0; col < cols; ++col)
        {
          if (grid[row, col] == 0)
            continue;

          int pathLength = GetPathFor(row, col, grid, new HashSet<Point>());

          if (pathLength > maxPathLength)
            maxPathLength = pathLength;
        }
      }

      return maxPathLength;
    }

    private static int GetPathFor(int row, int col, int[,] grid, HashSet<Point> path)
    {
      if (!IsAccessible(row, col, grid, path))
        return path.Count;

      var point = new Point(row, col);
      path.Add(point);
      
      int max = path.Count;
      
      max = Math.Max(max, GetPathFor(row, col - 1, grid, path));
      max = Math.Max(max, GetPathFor(row, col + 1, grid, path));
      max = Math.Max(max, GetPathFor(row - 1, col, grid, path));
      max = Math.Max(max, GetPathFor(row + 1, col, grid, path));

      path.Remove(point);

      return max;
    }

    private static bool IsAccessible(int row, int col, int[,] grid, HashSet<Point> path)
    {
      if (row < 0 || row > grid.GetUpperBound(0))
        return false;

      if (col < 0 || col > grid.GetUpperBound(1))
        return false;

      if (grid[row, col] == 0)
        return false;

      if (path.Contains(new Point(row, col)))
        return false;

      return true;
    }
  }
}