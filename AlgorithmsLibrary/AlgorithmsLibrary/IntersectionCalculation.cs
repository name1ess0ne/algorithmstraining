﻿using System.Drawing;

namespace AlgorithmsLibrary
{
  public static class IntersectionCalculation
  {
    public static bool IsRectanglesIntersect(Rectangle rect1, Rectangle rect2)
    {
      return rect1.X < rect2.X + rect2.Width &&
             rect2.X < rect1.X + rect1.Width &&
             rect1.Y < rect2.Y + rect2.Height &&
             rect2.Y < rect1.Y + rect1.Height;
    }
  }
}