﻿// Write a function that takes a list of integers and returns the maximum 
// product that can be derived from any three integers in the list. 
// An ArgumentException is thrown for any input that will not produce a valid Int32 product. 
// For the sample input [10, 3, 0, -2, 12, 5, 1, 1, 4] 
// The expected result is the product of 10, 12, and 5: 10*12*5 = 600 

using System;
using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsLibrary
{
    public static class MaxProductOfIntegers
    {
        public static int MaxThreeProduct(IList<int> input)
        {
            if (input == null || input.Count < 3)
                throw new ArgumentException();

            //init
            var arrayOfMaximums = new int[3];
            var arrayOfMinimums = new int[2];

            for (int i = 0; i < arrayOfMaximums.Length; ++i)
            {
                arrayOfMaximums[i] = int.MinValue;
            }

            for (int i = 0; i < arrayOfMinimums.Length; ++i)
            {
                arrayOfMinimums[i] = int.MaxValue;
            }

            //algorithm
            for (int i = 0; i < input.Count; ++i)
            {
                AccumulateMinElements(arrayOfMinimums, input[i]);//here will be accumulated minimums, also a negative values
                AccumulateMaxElements(arrayOfMaximums, input[i]);
            }

            int x1 = CalculateProduct(arrayOfMaximums);
            //calculate product of two minimums and one maximum, if both minimums is negative, we can get bigger value than x1
            int x2 = CalculateProduct(arrayOfMaximums.Max(), arrayOfMinimums[0], arrayOfMinimums[1]);

            return Math.Max(x1, x2);
        }

        private static int CalculateProduct(params int[] array)
        {
            if (IsOverflow(array))
                throw new ArgumentException();

            int result = 1;

            for (int i = 0; i < array.Length; i++)
            {
                result *= array[i];
            }

            return result;
        }

        private static void AccumulateMinElements(IList<int> array, int value)
        {
            int indexOfMax = 0;

            for (int i = indexOfMax; i < array.Count; ++i)
            {
                if (array[i] > array[indexOfMax])
                {
                    indexOfMax = i;
                }
            }

            if (array[indexOfMax] > value)
                array[indexOfMax] = value;
        }

        private static void AccumulateMaxElements(IList<int> array, int value)
        {
            int indexOfMin = 0;

            for (int i = indexOfMin; i < array.Count; ++i)
            {
                if (array[i] < array[indexOfMin])
                {
                    indexOfMin = i;
                }
            }

            if (array[indexOfMin] < value)
                array[indexOfMin] = value;
        }

        private static bool IsOverflow(IList<int> array)
        {
            int num = int.MaxValue;

            for (int i = 0; i < array.Count; ++i)
            {
                if (array[i] == 0)
                    continue;
                
                //num /= Math.Abs(array[i]);//will cause overflow exception if argument equals int.MinValue
                num /= array[i] < 0 ? -array[i] : array[i];
            }

            return num == 0;
        }
    }
}