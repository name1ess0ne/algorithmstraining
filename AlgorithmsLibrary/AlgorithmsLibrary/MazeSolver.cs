﻿using System.Collections.Generic;

namespace AlgorithmsLibrary
{
    public class MazeNode
    {
        public int Value { get; set; }
        public List<MazeNode> SubNodes { get; set; }

        public MazeNode()
        {
            SubNodes = new List<MazeNode>();
        }
    }

    public class MazeWay : List<int>
    {
        public override string ToString()
        {
            return string.Join("->", this);
        }
    }

    public static class MazeSolver
    {
        public static IEnumerable<MazeWay> SolveMaze(MazeNode root, int exitNodeValue)
        {
            return SolveMaze(root, exitNodeValue, new HashSet<MazeNode>());
        }

        private static IEnumerable<MazeWay> SolveMaze(MazeNode root, int exitNodeValue, HashSet<MazeNode> visited)
        {
            if (root == null)
                yield break;

            if (root.Value == exitNodeValue)
                yield return new MazeWay { root.Value };

            if (visited.Contains(root))
                yield break;

            visited.Add(root);

            if (root.SubNodes == null || root.SubNodes.Count == 0)
                yield break;

            foreach (MazeNode subNode in root.SubNodes)
            {
                IEnumerable<MazeWay> subWays = SolveMaze(subNode, exitNodeValue, visited);

                foreach (MazeWay subWay in subWays)
                {
                    subWay.Insert(0, root.Value);
                    yield return subWay;
                }
            }
        }
    }
}