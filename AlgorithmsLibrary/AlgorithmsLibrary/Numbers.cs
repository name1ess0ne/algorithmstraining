﻿using System;
using System.Collections.Generic;

namespace AlgorithmsLibrary
{
  public static class Numbers
  {
    public static IEnumerable<int> NumberToDigits(int number)
    {
      var digits = new Stack<int>();

      while (number >= 10)
      {
        digits.Push(number % 10);
        number = number / 10;
      }

      digits.Push(number);

      return digits;
    }

    public static IEnumerable<int> NumberToDigitsReversOrder(int number)
    {
      while (number >= 10)
      {
        yield return number % 10;
        number = number / 10;
      }

      yield return number;
    }

    public static int GetDigitByRank(int number, int rank)
    {
      int num = number / (int)Math.Pow(10, rank);
      return num % 10;
    }
  }
}