﻿//http://www.testdome.com/Questions/Csharp/Palindrome/1299?testId=18&testDifficulty=Easy
// Write a function that checks if a given sentence is a palindrome. 
// A palindrome is a word, phrase, verse, or sentence that reads the same backward or forward. 
// Only the order of English alphabet letters (A-Z and a-z) should be considered, other characters should be ignored.
//
// For example, IsPalindrome("Noel sees Leon.") should return true as spaces, period, 
// and case should be ignored resulting with "noelseesleon" which is a palindrome since it reads same backward and forward.

namespace AlgorithmsLibrary
{
    public class Palindrome
    {
        public static bool IsPalindrome(string str)
        {
            int i = 0;
            int j = str.Length - 1;

            while (i < j)
            {
                if (!IsChar(str[i]))
                {
                    i++;
                    continue;
                }

                if (!IsChar(str[j]))
                {
                    j--;
                    continue;
                }

                if (char.ToLowerInvariant(str[i]) != char.ToLowerInvariant(str[j]))
                    return false;

                //if (ToLower(str[i]) != ToLower(str[j]))
                //    return false;

                i++;
                --j;
            }

            return true;
        }

        private static bool IsChar(char ch)
        {
            return (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z');
        }

        private static char ToLower(char ch)
        {
            if (ch >= 'A' && ch <= 'Z')
            {
                ch += (char) ('a' - 'A');
            }

            return ch;
        }
    }
}