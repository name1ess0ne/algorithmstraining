﻿//Write function that returns count of all polindromes in the given string.
//For example: xabbaz => 2 (aa, abba); xabcbazy => 2 (bcb, abcba)
using System;

namespace AlgorithmsLibrary
{
  public static class PalindromeCounter
  {
    public static int CountPalindrome(string str)
    {
      int count = 0;

      for (int i = 0; i < str.Length; i++)
      {
        count += CountPalindrome(str, i, i + 1);
        count += CountPalindrome(str, i, i);
      }

      return count;
    }

    private static int CountPalindrome(string str, int leftRun, int rightRun)
    {
      int count = 0;

      while (leftRun >= 0 && rightRun < str.Length)
      {
        if (str[leftRun] != str[rightRun])
        {
          break;
        }

        if (rightRun != leftRun)
        {
          count++;
          //for debugging
          string palindrome = str.Substring(leftRun, rightRun - leftRun + 1);
          Console.WriteLine(palindrome);
        }

        --leftRun;
        ++rightRun;
      }

      return count;
    }
  }
}
