﻿//Write a function that provides change directory (cd) function for an abstract file system.

//Notes:
//- Root path is '/'.
//- Path separator is '/'.
//- Parent directory is addressable as "..".
//- Directory names consist only of English alphabet letters (A-Z and a-z).

//For example, new Path("/a/b/c/d").Cd("../x").CurrentPath should return "/a/b/c/x".

using System.Collections.Generic;

public class Path
{
    public Path(string path)
    {
        CurrentPath = path;
    }

    public string CurrentPath { get; private set; }

    public Path Cd(string newPath)
    {
        var newQueue = new Queue<string>(newPath.Split('/'));
        Stack<string> currentStack;

        if (newPath[0] == '/')
            currentStack = new Stack<string>();
        else
            currentStack = new Stack<string>(CurrentPath.Split('/'));

        while (newQueue.Count > 0)
        {
            string item = newQueue.Dequeue();
            if (item == "..")
            {
                if (currentStack.Peek() != "")
                    currentStack.Pop();
            }
            else
            {
                currentStack.Push(item);
            }
        }

        var invertedStack = new Stack<string>(currentStack);
        return new Path(string.Join("/", invertedStack));
    }

    //неправильное решение на строках, т.к. не сработает для ../x/../b/../f
    //public Path Cd(string newPath)
    //{
    //    int curPathIndex = CurrentPath.Length - 1;

    //    if (newPath[0] == '/')
    //        curPathIndex = 0;

    //    int i;
    //    for (i = 0; i < newPath.Length; ++i)
    //    {
    //        if (i + 1 >= newPath.Length)
    //            break;

    //        if (newPath[i] == '.' && newPath[i + 1] == '.')
    //        {
    //            curPathIndex = CurrentPath.LastIndexOf('/', curPathIndex);

    //            if (curPathIndex < 0)
    //                curPathIndex = 0;

    //            i++;
    //        }

    //        if (newPath[i] == '/')
    //            continue;
    //    }

    //    var result = CurrentPath.Substring(0, curPathIndex) + '/' + newPath.Substring(i);

    //    if (result != "/")
    //        result = result.TrimEnd('/');

    //    return new Path(result);
    //}
}