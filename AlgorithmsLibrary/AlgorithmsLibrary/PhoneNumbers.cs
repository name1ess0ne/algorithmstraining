﻿// A company needs to fix a set of customer phone numbers which were incorrectly stored. Phone numbers were mistakenly stored in 
// the following formats: 
//
// [Prefix ###][Area Code @@@][Line Number &&&&]
// ###@@@&&&&
// ###-@@@-&&&&
//
// The fix intends to normalize the phone numbers as: 
// @@@-###-&&&&
//
// The set of phone numbers impacted is large, so do your best to minimize additional storage. 
// Implement: 
// void Reformat( string[] phoneNumbers ); 

namespace AlgorithmsLibrary
{
  public static class PhoneNumbers
  {
    private static readonly char[] Buffer = new char[12];

    public static void Reformat(string[] phoneNumbers)
    {
      for (int i = 0; i < phoneNumbers.Length; i++)
      {
        phoneNumbers[i] = ReformatPhone1(phoneNumbers[i]);
      }
    }

    public static void Reformat2(string[] phoneNumbers)
    {
      for (int i = 0; i < phoneNumbers.Length; i++)
      {
        phoneNumbers[i] = ReformatPhone2(phoneNumbers[i]);
      }
    }

    private static string ReformatPhone1(string phoneNumber)
    {
      int cnt = 0;
      for (int i = 0; i < phoneNumber.Length; ++i)
      {
        if (phoneNumber[i] == '-')
          continue;

        if (cnt == 3 || cnt == 7)
        {
          Buffer[cnt] = '-';
          cnt++;
        }

        int offset = 0;
        if (cnt < 3)
        {
          offset = 4;
        }
        else if (cnt > 3 && cnt < 7)
        {
          offset = -4;
        }

        Buffer[cnt + offset] = phoneNumber[i];
        cnt++;
      }

      return new string(Buffer);
    }

    private static string ReformatPhone2(string phoneNumber)
    {
      int i = 0;
      int j = 0;

      while (i < phoneNumber.Length)
      {
        if (phoneNumber[i] == '-')
        {
          i++;
          continue;
        }

        if (j == 3 || j == 7)
        {
          Buffer[j] = '-';
          j++;
          continue;
        }

        int offset = 0;

        if (j < 3)
        {
          offset = 4;
        }
        else if (j > 3 && j < 7)
        {
          offset = -4;
        }

        Buffer[j + offset] = phoneNumber[i];

        i++;
        j++;
      }

      return new string(Buffer);
    }
  }
}