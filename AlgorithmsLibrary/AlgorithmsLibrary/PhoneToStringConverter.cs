﻿using System.Collections.Generic;
using System.Linq;

namespace AlgorithmsLibrary
{
  //Given phone, make function that returns all phone representation in a string. 
  //Each digit in phone should be represent as char from mobile phone button. 
  //Example: 1 сan be represent as a, b or c. 
  public static class PhoneToStringConverter
  {
    private static readonly Dictionary<int, List<string>> DigitToCharsMap = new Dictionary<int, List<string>>
    {
      {1, new List<string> {"a", "b", "c"}},
      {2, new List<string> {"d", "e", "f"}},
      {3, new List<string> {"g", "h", "i"}},
    };

    public static IEnumerable<string> PhoneToString(int phone)
    {
      List<int> digits = GetDigits(phone);
      return PhoneToString(digits, 0);
    }

    private static IEnumerable<string> PhoneToString(List<int> digits, int digitIndex)
    {
      int digit = digits[digitIndex];
      List<string> symbols = DigitToCharsMap[digit];

      if (digitIndex == digits.Count - 1)
        return symbols;

      var result = new List<string>();

      foreach (string tail in PhoneToString(digits, digitIndex + 1))
      {
        foreach (string symbol in symbols)
        {
          result.Add(symbol + tail);
        }
      }

      return result;
    }

    private static List<int> GetDigits(int num)
    {
      var result = new Stack<int>();

      while (num >= 10)
      {
        result.Push(num % 10);
        num = num / 10;
      }

      result.Push(num);

      return result.ToList();
    }
  }
}