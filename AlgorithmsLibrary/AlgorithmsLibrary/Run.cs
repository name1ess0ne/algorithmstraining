﻿namespace AlgorithmsLibrary
{
  public class Run
  {
    public static int IndexOfLongestRun(string str)
    {
      if (string.IsNullOrEmpty(str))
        return 0;

      int maxRunCount = 0;
      int index = 0;

      for (int i = 0; i < str.Length;)
      {
        int runCount = GetRunCount(str, i);

        if (maxRunCount < runCount)
        {
          maxRunCount = runCount;
          index = i;
        }

        i += runCount;
      }

      return index;
    }

    private static int GetRunCount(string str, int startIndex)
    {
      char ch = str[startIndex];
      int count = 1;

      for (int i = startIndex + 1; i < str.Length; ++i)
      {
        if (ch == str[i])
          count++;
        else
          break;
      }

      return count;
    }

    public static int IndexOfLongestRunTwo(string str)
    {
      int longestRunCount = 0;
      int currentRunCount = 0;
      int currentRunIndex = 0;
      int index = 0;

      for (int i = 0; i < str.Length; ++i)
      {
        if (str[currentRunIndex] == str[i])
        {
          currentRunCount++;
        }
        else
        {
          currentRunCount = 1;
          currentRunIndex = i;
        }

        if (longestRunCount < currentRunCount)
        {
          longestRunCount = currentRunCount;
          index = currentRunIndex;
        }
      }

      return index;
    }
  }
}