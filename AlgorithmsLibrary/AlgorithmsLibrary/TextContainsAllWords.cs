﻿using System.Collections.Generic;

namespace AlgorithmsLibrary
{
    public static class TextContainsAllWords
    {
        /// <summary>
        ///     Возвращает true, если пользуясь словарем <paramref name="words" /> можно составить слово <paramref name="text" />
        /// </summary>
        public static bool IsTextContainsAllWords(string text, string[] words)
        {
            var allWords = new HashSet<string>();

            foreach (string word in words)
            {
                allWords.Add(word);
            }

            return IsTextContainsAllWords(text, allWords);
        }

        private static bool IsTextContainsAllWords(string text, HashSet<string> words)
        {
            for (int i = 1; i <= text.Length; ++i)
            {
                string word = text.Substring(0, i);

                if (words.Contains(word))
                {
                    string tail = text.Substring(i);

                    if (string.IsNullOrEmpty(tail))
                        return true; //we reach the end of the text

                    if (IsTextContainsAllWords(tail, words))
                        return true;
                }
            }

            return false;
        }
    }
}