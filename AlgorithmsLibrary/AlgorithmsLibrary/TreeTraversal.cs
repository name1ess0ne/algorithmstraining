﻿using System;
using System.Collections.Generic;
using AlgorithmsLibrary.DataStructures;

namespace AlgorithmsLibrary
{
  public static class TreeTraversal
  {
    #region Pre-Order

    //root, left, right
    public static void PreOrder(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      action(root);
      PreOrder(root.Left, action);
      PreOrder(root.Right, action);
    }

    public static IEnumerable<Node> PreOrder(Node root)
    {
      if (root == null)
        yield break;

      yield return root;

      foreach (Node left in PreOrder(root.Left))
      {
        yield return left;
      }

      foreach (Node right in PreOrder(root.Right))
      {
        yield return right;
      }
    }

    public static void PreOrderIterative(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      Stack<Node> stack = new Stack<Node>();

      stack.Push(root);

      while (stack.Count > 0)
      {
        Node n = stack.Pop();

        action(n);

        if (n.Right != null)
          stack.Push(n.Right);

        if (n.Left != null)
          stack.Push(n.Left);
      }
    }

    #endregion

    #region In-Order

    //left, root, right
    public static void InOrder(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      InOrder(root.Left, action);
      action(root);
      InOrder(root.Right, action);
    }

    public static void InOrderIterative(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      Stack<Node> stack = new Stack<Node>();
      Node node = root;

      while (node != null)
      {
        stack.Push(node);
        node = node.Left;
      }

      while (stack.Count > 0)
      {
        node = stack.Pop();
        action(node);

        node = node.Right;

        while (node != null)
        {
          stack.Push(node);
          node = node.Left;
        }
      }
    }

    public static void InOrderIterativeWithCache(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      HashSet<Node> visited = new HashSet<Node>();

      Stack<Node> stack = new Stack<Node>();
      stack.Push(root);

      while (stack.Count > 0)
      {
        Node n = stack.Peek();

        if (n.Left != null && !visited.Contains(n.Left))
        {
          stack.Push(n.Left);
        }
        else
        {
          n = stack.Pop();
          action(n);
          visited.Add(n);

          if (n.Right != null)
            stack.Push(n.Right);
        }
      }
    }

    #endregion

    #region Post-Order

    //left, right, root
    public static void PostOrder(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      PostOrder(root.Left, action);
      PostOrder(root.Right, action);
      action(root);
    }

    public static void PostOrderIterative(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      Stack<Node> stack = new Stack<Node>();

      stack.Push(root);

      Node prev = null;

      while (stack.Count > 0)
      {
        Node curr = stack.Peek();

        if (prev == null || prev.Left == curr || prev.Right == curr)
        {
          if (curr.Left != null)
            stack.Push(curr.Left);
          else if (curr.Right != null)
            stack.Push(curr.Right);
        }
        else if (curr.Left == prev)
        {
          if (curr.Right != null)
            stack.Push(curr.Right);
        }
        else
        {
          action(curr);
          stack.Pop();
        }

        prev = curr;
      }
    }

    public static void PostOrderIterativeTwoStacks(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      Stack<Node> stack = new Stack<Node>();
      Stack<Node> outputStack = new Stack<Node>();

      stack.Push(root);

      while (stack.Count > 0)
      {
        Node curr = stack.Pop();
        outputStack.Push(curr);

        if (curr.Left != null)
          stack.Push(curr.Left);

        if (curr.Right != null)
          stack.Push(curr.Right);
      }

      while (outputStack.Count > 0)
      {
        action(outputStack.Pop());
      }
    }

    #endregion

    #region Level-Order

    public static void LevelOrder(Node root, Action<Node> action)
    {
      if (root == null)
        return;

      Queue<Node> queue = new Queue<Node>();

      queue.Enqueue(root);

      while (queue.Count > 0)
      {
        Node n = queue.Dequeue();

        action(n);

        if (n.Left != null)
          queue.Enqueue(n.Left);

        if (n.Right != null)
          queue.Enqueue(n.Right);
      }
    }

    #endregion
  }
}