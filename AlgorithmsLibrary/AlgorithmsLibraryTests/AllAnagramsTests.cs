﻿using System.Collections.Generic;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class AllAnagramsTests
  {
    [Test]
    public void GetAllAnagramsTest()
    {
      var expectedResult = new[] {"aabb", "abab", "abba", "baab", "baba", "bbaa"};

      ICollection<string> anagrams = AllAnagrams.GetAllAnagrams("abba");

      CollectionAssert.AreEquivalent(expectedResult, anagrams);
    }

    [Test]
    public void GetAllAnagramsTest1()
    {
      var expectedResult = new[] {"abc", "bca", "acb", "cba", "cab", "bac"};

      ICollection<string> anagrams = AllAnagrams.GetAllAnagrams("abc");

      CollectionAssert.AreEquivalent(expectedResult, anagrams);
    }

    [Test]
    public void GetAllAnagramsTest2()
    {
      var expectedResult = new[]
      {
        "abcd", "bacd", "bcad", "bcda", "acbd", "cabd", "cbad", "cbda", "acdb", "cadb", "cdab", "cdba", "abdc", "badc",
        "bdac", "bdca", "adbc", "dabc", "dbac", "dbca", "adcb", "dacb", "dcab", "dcba"
      };

      ICollection<string> anagrams = AllAnagrams.GetAllAnagrams("abcd");

      //string sttttr = string.Join(",", anagrams.Select(x => "\"" + x + "\""));

      CollectionAssert.AreEquivalent(expectedResult, anagrams);
    }
  }
}