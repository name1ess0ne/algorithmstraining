﻿using System.Collections.Generic;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class AllValidParenthesesCombinationsTests
  {
    [Test]
    public void GenerateParenthases_Count0_Test()
    {
      var expected = new List<string>();

      var resultFast = AllValidParenthesesCombinations.GenerateParenthasesFast(0);
      var resultSimple = AllValidParenthesesCombinations.GenerateParenthasesSimple(0);

      CollectionAssert.AreEquivalent(expected, resultFast);
      CollectionAssert.AreEquivalent(expected, resultSimple);
    }

    [Test]
    public void GenerateParenthases_Count1_Test()
    {
      var expected = new List<string> { "()" };

      var resultFast = AllValidParenthesesCombinations.GenerateParenthasesFast(1);
      var resultSimple = AllValidParenthesesCombinations.GenerateParenthasesSimple(1);

      CollectionAssert.AreEquivalent(expected, resultFast);
      CollectionAssert.AreEquivalent(expected, resultSimple);
    }

    [Test]
    public void GenerateParenthases_Count2_Test()
    {
      var expected = new List<string>
      {
        "(())",
        "()()"
      };

      var resultFast = AllValidParenthesesCombinations.GenerateParenthasesFast(2);
      var resultSimple = AllValidParenthesesCombinations.GenerateParenthasesSimple(2);

      CollectionAssert.AreEquivalent(expected, resultFast);
      CollectionAssert.AreEquivalent(expected, resultSimple);
    }

    [Test]
    public void GenerateParenthases_Count3_Test()
    {
      var expected = new List<string>
      {
        "((()))",
        "(()())",
        "(())()",
        "()(())",
        "()()()"
      };

      var resultFast = AllValidParenthesesCombinations.GenerateParenthasesFast(3);
      var resultSimple = AllValidParenthesesCombinations.GenerateParenthasesSimple(3);

      CollectionAssert.AreEquivalent(expected, resultFast);
      CollectionAssert.AreEquivalent(expected, resultSimple);
    }
  }
}