﻿using NUnit.Framework;
using TrainingProblems;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class AreAnagramsTests
    {
        [TestCase("momdad", "dadmom", ExpectedResult = true)]
        [TestCase("momdad", "momdad", ExpectedResult = true)]
        [TestCase("", "", ExpectedResult = true)]
        [TestCase("a", "a", ExpectedResult = true)]
        [TestCase("a", "b", ExpectedResult = false)]
        [TestCase("abc", "bca", ExpectedResult = true)]
        public bool AreStringsAnagramsTests(string a, string b)
        {
            return AreAnagrams.AreStringsAnagrams(a, b);
        }

        [TestCase("momdad", "dadmom", ExpectedResult = true)]
        [TestCase("momdad", "momdad", ExpectedResult = true)]
        [TestCase("", "", ExpectedResult = true)]
        [TestCase("a", "a", ExpectedResult = true)]
        [TestCase("a", "b", ExpectedResult = false)]
        [TestCase("abc", "bca", ExpectedResult = true)]
        [TestCase("aabbbb", "bbaaaa", ExpectedResult = false)]
        public bool AreStringsAnagramsForAscii(string a, string b)
        {
            return AreAnagrams.AreStringsAnagramsForAscii(a, b);
        }

        [TestCase("momdad", "dadmom", ExpectedResult = true)]
        [TestCase("momdad", "momdad", ExpectedResult = false)]
        [TestCase("", "", ExpectedResult = true)]
        [TestCase("abc", "", ExpectedResult = false)]
        [TestCase("abc", "c", ExpectedResult = false)]
        public bool IsInvertedStringsTests(string a, string b)
        {
            return AreAnagrams.IsInvertedStrings(a, b);
        }
    }
}