﻿using System.Linq;
using AlgorithmsLibrary.BaseAlgorithms;
using NUnit.Framework;

namespace AlgorithmsLibraryTests.BaseAlgorithms
{
  [TestFixture]
  public class SortingTests
  {
    [Test]
    public void SortTest()
    {
      int[] initialArray = { 3, 8, 967, 6, -5, 5, 7, -8 };
      int[] expectedArray = { -8, -5, 3, 5, 6, 7, 8, 967 };

      SortAll(initialArray, expectedArray);
    }

    [Test]
    public void SortEmptyArrayTest()
    {
      int[] initialArray = { };
      int[] expectedArray = { };

      SortAll(initialArray, expectedArray);
    }

    private void SortAll(int[] initialArray, int[] expectedArray)
    {
      // Bubble Sort
      int[] bubbleArray = initialArray.ToArray();
      BubbleSort.Sort(bubbleArray);
      CollectionAssert.AreEqual(expectedArray, bubbleArray, "Bubble sort error");

      // Selection Sort
      int[] selectionArray = initialArray.ToArray();
      SelectionSort.Sort(selectionArray);
      CollectionAssert.AreEqual(expectedArray, selectionArray, "Selection sort error");

      // Selection Sort Inlined
      int[] selectionArray2 = initialArray.ToArray();
      SelectionSort.SortInlined(selectionArray2);
      CollectionAssert.AreEqual(expectedArray, selectionArray2, "Selection sort error");

      // Insertion Sort
      int[] insertionArray = initialArray.ToArray();
      InsertionSort.Sort(insertionArray);
      CollectionAssert.AreEqual(expectedArray, insertionArray, "Insertion sort error");

      // Merge Sort
      int[] mergeArray = initialArray.ToArray();
      MergeSort.Sort(mergeArray);
      CollectionAssert.AreEqual(expectedArray, mergeArray, "Merge sort error");

      // Quick Sort
      int[] quickArray = initialArray.ToArray();
      MergeSort.Sort(quickArray);
      CollectionAssert.AreEqual(expectedArray, quickArray, "Quick sort error");

      //Heap sort
      int[] heapArray = initialArray.ToArray();
      HeapSort.Sort(heapArray);
      CollectionAssert.AreEqual(expectedArray, heapArray, "Heap sort error");
    }
  }
}