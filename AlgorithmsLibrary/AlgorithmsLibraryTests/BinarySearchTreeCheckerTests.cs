﻿using AlgorithmsLibrary;
using AlgorithmsLibrary.DataStructures;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class BinarySearchTreeCheckerTests
  {
    [Test]
    public void IsValidBSTTest_InvalidBSTWithEqualValueInTheRightSubtree_Fail()
    {
      var root = new Node
      {
        Value = 3,
        Right = new Node
        {
          Value = 7,
          Right = new Node {Value = 7}
        }
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_InvalidBSTWithEqualValueInTheRight_Fail()
    {
      var root = new Node
      {
        Left = new Node {Value = 2},
        Value = 5,
        Right = new Node {Value = 5}
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_InvalidBSTWithGreaterValueInTheLeftSubtree_Fail()
    {
      var root = new Node
      {
        Left = new Node
        {
          Left = new Node {Value = 1},
          Value = 2,
          Right = new Node {Value = 10}
        },
        Value = 3,
        Right = new Node {Value = 7}
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_InvalidBSTWithSameValuesInAllTree_Fail()
    {
      var root = new Node
      {
        Left = new Node {Value = 1},
        Value = 1,
        Right = new Node {Value = 1,}
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_InvalidBSTWithSmallerValueInTheRightSubtree_Fail()
    {
      var root = new Node
      {
        Left = new Node {Value = 2},
        Value = 3,
        Right = new Node
        {
          Left = new Node {Value = -1},
          Value = 7,
          Right = new Node {Value = 10}
        }
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_InvalidBST_Fail()
    {
      var root = new Node
      {
        Left = new Node {Value = 10},
        Value = 5,
        Right = new Node {Value = 1}
      };

      Assert.IsFalse(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_RegularValidBST_Success()
    {
      var root = new Node
      {
        Left = new Node {Value = 1},
        Value = 2,
        Right = new Node {Value = 3}
      };

      Assert.IsTrue(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_ValidBSTWithEqualLeftValue_Success()
    {
      var root = new Node
      {
        Left = new Node {Value = 5},
        Value = 5,
        Right = new Node {Value = 8}
      };

      Assert.IsTrue(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_ValidBSTWithEqualValueInLeftSubtree_Success()
    {
      var root = new Node
      {
        Left = new Node
        {
          Left = new Node {Value = 1},
          Value = 2,
          Right = new Node {Value = 3}
        },
        Value = 3
      };

      Assert.IsTrue(BinarySearchTreeChecker.IsValidBST(root));
    }

    [Test]
    public void IsValidBSTTest_ValidBSTWithNegativeValue_Success()
    {
      var root = new Node {Value = -5};
      Assert.IsTrue(BinarySearchTreeChecker.IsValidBST(root));
    }
  }
}