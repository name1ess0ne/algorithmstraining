﻿using AlgorithmsLibrary;
using AlgorithmsLibrary.DataStructures;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class BinaryTreeCommonAncestorTests
    {
        [Test]
        public void FindCommonAncestorTest()
        {
            var node1 = new Node();
            var node2 = new Node();
            var commonAncestor = new Node {Left = node1, Right = new Node {Left = node2}};

            var root = new Node
            {
                Left = commonAncestor,
                Right = new Node()
            };

            Node result = BinaryTreeCommonAncestor.FindCommonAncestor(root, node1, node2);
            Node resultOptimized = BinaryTreeCommonAncestor.FindCommonAncestorOptimized(root, node1, node2);

            Assert.AreEqual(commonAncestor, result);
            Assert.AreEqual(commonAncestor, resultOptimized);
        }

        [Test]
        public void FindCommonAncestor_RootIsParent_Test()
        {
            var node1 = new Node();
            var node2 = new Node();
            
            var root = new Node
            {
                Left = node1,
                Right = node2
            };

            Node result = BinaryTreeCommonAncestor.FindCommonAncestor(root, node1, node2);
            Node resultOptimized = BinaryTreeCommonAncestor.FindCommonAncestorOptimized(root, node1, node2);

            Assert.AreEqual(root, result);
            Assert.AreEqual(root, resultOptimized);
        }

        [Test]
        public void FindCommonAncestor_NoAncestor_Test()
        {
            var node1 = new Node();
            var node2 = new Node();

            var root = new Node
            {
                Left = node1,
                Right = new Node()
            };

            Node result = BinaryTreeCommonAncestor.FindCommonAncestor(root, node1, node2);
            Node resultOptimized = BinaryTreeCommonAncestor.FindCommonAncestorOptimized(root, node1, node2);

            Assert.IsNull(result);
            Assert.IsNull(resultOptimized);
        }

        [Test]
        public void FindCommonAncestor_OneNode_Test()
        {
            var root = new Node
            {
                Left = new Node(),
                Right = new Node()
            };

            Node result = BinaryTreeCommonAncestor.FindCommonAncestor(root, root, root);
            Node resultOptimized = BinaryTreeCommonAncestor.FindCommonAncestorOptimized(root, root, root);

            Assert.AreEqual(root, result);
            Assert.AreEqual(root, resultOptimized);
        }
    }
}