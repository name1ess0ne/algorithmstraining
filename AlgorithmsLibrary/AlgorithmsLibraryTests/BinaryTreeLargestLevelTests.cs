﻿using System;
using AlgorithmsLibrary;
using AlgorithmsLibrary.DataStructures;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class BinaryTreeLargestLevelTests
  {
    [Test]
    public void FindLargestLevel()
    {
      var root = new Node
      {
        Left = new Node
        {
          Left = new Node { Value = 1 },
          Value = 2,
          Right = new Node { Value = 10 }
        },
        Value = 3,
        Right = new Node
        {
          Value = 7,
          Left = new Node
          {
            Value = 1,
            Right = new Node { Value = 4 }
          }
        }
      };

      Tuple<int, int> levelCount = BinaryTreeLargestLevel.FindLargestLevel(root);

      Assert.AreEqual(2, levelCount.Item1);
      Assert.AreEqual(3, levelCount.Item2);
    }
  }
}
