﻿using System.Collections.Generic;
using AlgorithmsLibrary.DataStructures;
using NUnit.Framework;

namespace AlgorithmsLibraryTests.DataStructures
{
  [TestFixture]
  public class HeapTests
  {
    [Test]
    public void HeapTest()
    {
      var maxHeap = new Heap<int>((x, y) => x.CompareTo(y));

      maxHeap.Push(1);
      maxHeap.Push(2);
      maxHeap.Push(5);
      maxHeap.Push(3);

      Assert.AreEqual(5, maxHeap.Pop());
      Assert.AreEqual(3, maxHeap.Pop());
      Assert.AreEqual(2, maxHeap.Pop());
      Assert.AreEqual(1, maxHeap.Pop());
    }

    [Test]
    public void HeapCreateTest()
    {
      var maxHeap = new Heap<int>((x, y) => x.CompareTo(y), new[] { 0, 78, -1, 457, -7, -8, -5 });

      Assert.AreEqual(457, maxHeap.Pop());
      Assert.AreEqual(78, maxHeap.Pop());
      Assert.AreEqual(0, maxHeap.Pop());
      Assert.AreEqual(-1, maxHeap.Pop());
      Assert.AreEqual(-5, maxHeap.Pop());
      Assert.AreEqual(-7, maxHeap.Pop());
      Assert.AreEqual(-8, maxHeap.Pop());
    }

    [Test]
    public void HeapCreateTest2()
    {
      var maxHeap = new Heap<int>((x, y) => x.CompareTo(y), new[] { -8, -5 });

      Assert.AreEqual(-5, maxHeap.Pop());
      Assert.AreEqual(-8, maxHeap.Pop());

      //var maxHeap = new Heap<int>((x, y) => x.CompareTo(y), new[] { 3, 8, 967, 6, -5, 5, 7, -8 });

      //Assert.AreEqual(967, maxHeap.Pop());
      //Assert.AreEqual(8, maxHeap.Pop());
      //Assert.AreEqual(7, maxHeap.Pop());
      //Assert.AreEqual(6, maxHeap.Pop());
      //Assert.AreEqual(5, maxHeap.Pop());
      //Assert.AreEqual(3, maxHeap.Pop());
      //Assert.AreEqual(-5, maxHeap.Pop());
      //Assert.AreEqual(-8, maxHeap.Pop());
    }

    [Test]
    public void HeapKElementsTest()
    {
      int[] items = { 1, 5, 1, -100, -99, 0, -5, 99, 4, -99, 10, 2, 423, 32, 0, -455, 2, 455, 21 };
      var maxHeap = new Heap<int>((x, y) => -1 * x.CompareTo(y), items);

      var result = new List<int>();

      int count = maxHeap.Count;
      for (int i = 0; i < count; i++)
      {
        result.Add(maxHeap.Pop());
      }

      CollectionAssert.AreEqual(new[] { -455, -100, -99, -99, -5, 0, 0, 1, 1, 2, 2, 4, 5, 10, 21, 32, 99, 423, 455 }, result);
    }
  }
}
