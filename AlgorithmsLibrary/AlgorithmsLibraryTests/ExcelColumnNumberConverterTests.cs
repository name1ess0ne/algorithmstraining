﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class ExcelColumnNumberConverterTests
  {
    [TestCase(1, ExpectedResult = "A")]
    [TestCase(10, ExpectedResult = "J")]
    [TestCase(27, ExpectedResult = "AA")]
    [TestCase(28, ExpectedResult = "AB")]
    [TestCase(53, ExpectedResult = "BA")]
    [TestCase(702, ExpectedResult = "ZZ")]
    [TestCase(703, ExpectedResult = "AAA")]
    [TestCase(704, ExpectedResult = "AAB")]
    public string NumberToStringTest(int number)
    {
      //for (int i = 0; i < 1000; i++)
      //{
      //  System.Diagnostics.Debug.Print("[" + i + "] " + ExcelColumnNumberConverter.NumberToString(i));
      //}

      return ExcelColumnNumberConverter.NumberToString(number);
    }

    [TestCase(1, ExpectedResult = "A")]
    [TestCase(10, ExpectedResult = "J")]
    [TestCase(27, ExpectedResult = "AA")]
    [TestCase(28, ExpectedResult = "AB")]
    [TestCase(53, ExpectedResult = "BA")]
    [TestCase(702, ExpectedResult = "ZZ")]
    [TestCase(703, ExpectedResult = "AAA")]
    [TestCase(704, ExpectedResult = "AAB")]
    public string ColumnAddressTest(int number)
    {
      //for (int i = 0; i < 1000; i++)
      //{
      //  System.Diagnostics.Debug.Print("[" + i + "] " + ExcelColumnNumberConverter.ColumnAddress(i));
      //}

      return ExcelColumnNumberConverter.ColumnAddress(number);
    }

    [TestCase("A", ExpectedResult = 1)]
    [TestCase("J", ExpectedResult = 10)]
    [TestCase("AA", ExpectedResult = 27)]
    [TestCase("AB", ExpectedResult = 28)]
    [TestCase("BA", ExpectedResult = 53)]
    [TestCase("ZZ", ExpectedResult = 702)]
    [TestCase("AAA", ExpectedResult = 703)]
    [TestCase("AAB", ExpectedResult = 704)]
    public int ColumnNumberTest(string columnAddress)
    {
      return ExcelColumnNumberConverter.ColumnNumber(columnAddress);
    }
  }
}