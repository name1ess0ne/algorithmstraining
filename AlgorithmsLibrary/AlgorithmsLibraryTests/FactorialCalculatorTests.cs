﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class FactorialCalculatorTests
  {
    [TestCase(0, ExpectedResult = 1)]
    [TestCase(1, ExpectedResult = 1)]
    [TestCase(4, ExpectedResult = 24)]
    [TestCase(5, ExpectedResult = 120)]
    public int FactorialTest(int num)
    {
      return FactorialCalculator.Factorial(num);
    }

    [TestCase(0, ExpectedResult = 1)]
    [TestCase(1, ExpectedResult = 1)]
    [TestCase(4, ExpectedResult = 24)]
    [TestCase(5, ExpectedResult = 120)]
    public int FactorialStackTest(int num)
    {
      return FactorialCalculator.FactorialStack(num);
    }
  }
}