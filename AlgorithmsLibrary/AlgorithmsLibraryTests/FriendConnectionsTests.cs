﻿using System.Linq;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class FriendConnectionsTests
    {
        [Test]
        public void GetFriendsOfDegreeTest()
        {
            var a = new Member("A");
            var b = new Member("B");
            var c = new Member("C");

            a.AddFriend(b);
            b.AddFriend(c);

            CollectionAssert.AreEquivalent(new[] { c }, FriendConnections.GetFriendsOfDegreeQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { c }, FriendConnections.GetFriendsOfDegreePureQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { c }, FriendConnections.GetFriendsOfDegreeTwoList(a, 2));
            CollectionAssert.AreEquivalent(new[] { c }, FriendConnections.GetFriendsOfDegreeTwoQueue(a, 2));
        }

        [Test]
        public void GetFriendsOfDegreeTest1()
        {
            var a = new Member("A");
            var b = new Member("B");
            var c = new Member("C");
            var d = new Member("D");
            var e = new Member("E");

            a.AddFriend(b);
            a.AddFriend(d);
            b.AddFriend(c);
            b.AddFriend(d);
            b.AddFriend(e);

            CollectionAssert.AreEquivalent(new[] { c, e }, FriendConnections.GetFriendsOfDegreeQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { c, e }, FriendConnections.GetFriendsOfDegreePureQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { c, e }, FriendConnections.GetFriendsOfDegreeTwoList(a, 2));
            CollectionAssert.AreEquivalent(new[] { c, e }, FriendConnections.GetFriendsOfDegreeTwoQueue(a, 2));
        }

        [Test]
        public static void MutualFriend()
        {
            Member a = new Member("A");
            Member b = new Member("B");
            Member c = new Member("C");
            Member d = new Member("D");

            a.AddFriend(b);
            a.AddFriend(c);
            c.AddFriend(d);
            b.AddFriend(d);

            Assert.AreSame(d, FriendConnections.GetFriendsOfDegreeQueue(a, 2).Single());
            Assert.AreSame(d, FriendConnections.GetFriendsOfDegreePureQueue(a, 2).Single());
            Assert.AreSame(d, FriendConnections.GetFriendsOfDegreeTwoList(a, 2).Single());
            Assert.AreSame(d, FriendConnections.GetFriendsOfDegreeTwoQueue(a, 2).Single());
        }

        [Test]
        public static void AllDegreeFriends()
        {
            Member a = new Member("A");
            Member b = new Member("B");
            Member c = new Member("C");
            Member d = new Member("D");
            Member e = new Member("E");

            a.AddFriend(b);
            a.AddFriend(c);
            c.AddFriend(d);
            b.AddFriend(d);
            c.AddFriend(e);

            CollectionAssert.AreEquivalent(new[] { d, e }, FriendConnections.GetFriendsOfDegreeQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { d, e }, FriendConnections.GetFriendsOfDegreePureQueue(a, 2));
            CollectionAssert.AreEquivalent(new[] { d, e }, FriendConnections.GetFriendsOfDegreeTwoList(a, 2));
            CollectionAssert.AreEquivalent(new[] { d, e }, FriendConnections.GetFriendsOfDegreeTwoQueue(a, 2));
        }
    }
}