﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class FrogTests
    {
        //[TestCase(3, ExpectedResult = 3)]
        //[TestCase(35, ExpectedResult = 14930352)]
        [TestCase(5, ExpectedResult = 8)]
        //[TestCase(0, ExpectedResult = 0)]
        public int NumberOfWaysTest(int n)
        {
            return Frog.NumberOfWays(n);
        }

        [TestCase(3, ExpectedResult = 3)]
        [TestCase(35, ExpectedResult = 14930352)]
        [TestCase(5, ExpectedResult = 8)]
        [TestCase(0, ExpectedResult = 0)]
        public int NumberOfWaysStackTest(int n)
        {
            return Frog.NumberOfWaysStack(n);
        }
    }
}
