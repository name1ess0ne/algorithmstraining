﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class GoGameTests
    {
        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMove_FreeArea_Test(bool isBlackStone)
        {
            var blackStones = new bool[5, 5]; //[row, col]
            var whiteStones = new bool[5, 5]; //[row, col]

            bool result = GoGame.IsLegalMove(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMove_Atari_Test(bool isBlackStone)
        {
            bool[,] blackStones =  //[row, col]
            {
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, true,  false, true, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
            };

            var whiteStones = new bool[5, 5]; //[row, col]

            bool result = GoGame.IsLegalMove(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = false)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMove_Surrounded_Test(bool isBlackStone)
        {
            bool[,] blackStones =//[row, col]
            {
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, true,  false, true, false},
                {false, false, true,  false, false},
                {false, false, false, false, false}
            };

            var whiteStones = new bool[5, 5];//[row, col]

            bool result = GoGame.IsLegalMove(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMove_ChainAtari_Test(bool isBlackStone)
        {
            bool[,] blackStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, true,  true,  false, false},
                {false, true,  false, false, false, false},
                {false, true,  false, true,  false, false},
                {false, false, true,  false, false, false}
            };

            bool[,] whiteStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, false, false, false, false},
                {false, false, true, false, false, false},
                {false, false, true, false, false, false},
                {false, false, false, false, false, false}
            };

            bool result = GoGame.IsLegalMove(isBlackStone, 2, 3, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = false)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMove_ChainSurrounded_Test(bool isBlackStone)
        {
            bool[,] blackStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, true,  true,  false, false},
                {false, true,  false, false, true, false},
                {false, true,  false, true,  false, false},
                {false, false, true,  false, false, false}
            };

            bool[,] whiteStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, false, false, false, false},
                {false, false, true, false, false, false},
                {false, false, true, false, false, false},
                {false, false, false, false, false, false}
            };

            bool result = GoGame.IsLegalMove(isBlackStone, 2, 3, blackStones, whiteStones);
            return result;
        }

        //-----------------

        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMoveAlternative_FreeArea_Test(bool isBlackStone)
        {
            var blackStones = new bool[5, 5]; //[row, col]
            var whiteStones = new bool[5, 5]; //[row, col]

            bool result = GoGame.IsLegalMoveAlternative(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMoveAlternative_Atari_Test(bool isBlackStone)
        {
            bool[,] blackStones =  //[row, col]
            {
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, true,  false, true, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
            };

            var whiteStones = new bool[5, 5]; //[row, col]

            bool result = GoGame.IsLegalMoveAlternative(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = false)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMoveAlternative_Surrounded_Test(bool isBlackStone)
        {
            bool[,] blackStones =//[row, col]
            {
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, true,  false, true, false},
                {false, false, true,  false, false},
                {false, false, false, false, false}
            };

            var whiteStones = new bool[5, 5];//[row, col]

            bool result = GoGame.IsLegalMoveAlternative(isBlackStone, 2, 2, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = true)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMoveAlternative_ChainAtari_Test(bool isBlackStone)
        {
            bool[,] blackStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, true,  true,  false, false},
                {false, true,  false, false, false, false},
                {false, true,  false, true,  false, false},
                {false, false, true,  false, false, false}
            };

            bool[,] whiteStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, false, false, false, false},
                {false, false, true, false, false, false},
                {false, false, true, false, false, false},
                {false, false, false, false, false, false}
            };

            bool result = GoGame.IsLegalMoveAlternative(isBlackStone, 2, 3, blackStones, whiteStones);
            return result;
        }

        [TestCase(false, ExpectedResult = false)]
        [TestCase(true, ExpectedResult = true)]
        public bool IsLegalMoveAlternative_ChainSurrounded_Test(bool isBlackStone)
        {
            bool[,] blackStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, true,  true,  false, false},
                {false, true,  false, false, true, false},
                {false, true,  false, true,  false, false},
                {false, false, true,  false, false, false}
            };

            bool[,] whiteStones = //[row, col]
            {
                {false, false, false, false, false, false},
                {false, false, false, false, false, false},
                {false, false, true, false, false, false},
                {false, false, true, false, false, false},
                {false, false, false, false, false, false}
            };

            bool result = GoGame.IsLegalMoveAlternative(isBlackStone, 2, 3, blackStones, whiteStones);
            return result;
        }
    }
}