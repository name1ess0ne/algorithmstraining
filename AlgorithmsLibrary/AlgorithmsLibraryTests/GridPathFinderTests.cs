﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class GridPathFinderTests
  {
    [Test]
    public void GetLongestPathTest()
    {
      int[,] grid =
      {
        {1, 1, 0, 0, 0},
        {0, 1, 1, 1, 1},
        {0, 1, 0, 0, 1},
        {1, 1, 1, 1, 1},
        {1, 0, 1, 0, 0}
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(13, result);
    }

    [Test]
    public void GetLongestPathTest1()
    {
      int[,] grid =
      {
        {0, 0, 0, 0, 0},
        {0, 1, 1, 1, 0},
        {0, 1, 0, 1, 0},
        {0, 1, 1, 1, 0},
        {0, 0, 0, 0, 0}
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(8, result);
    }

    [Test]
    public void GetLongestPathTest2()
    {
      int[,] grid =
      {
        {1, 1, 1},
        {0, 1, 0},
        {1, 1, 1},
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(5, result);
    }

    [Test]
    public void GetLongestPathTest3()
    {
      int[,] grid =
      {
        {0, 0, 0, 0, 0},
        {0, 0, 0, 1, 0},
        {0, 0, 1, 0, 0},
        {0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0}
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(1, result);
    }

    [Test]
    public void GetLongestPathTest4()
    {
      int[,] grid =
      {
        {0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0},
        {0, 1, 0, 0, 0},
        {0, 0, 0, 0, 0}
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(1, result);
    }

    [Test]
    public void GetLongestPathTest5()
    {
      int[,] grid =
      {
        {1, 1, 1},
        {1, 1, 1},
        {1, 1, 1},
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(9, result);
    }

    [Test]
    public void GetLongestPathTest6()
    {
      int[,] grid =
      {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
      };

      int result = GridPathFinder.GetLongestPath(grid);

      Assert.AreEqual(0, result);
    }
  }
}