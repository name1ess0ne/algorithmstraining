﻿using System.Drawing;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class IntersectionCalculationTests
  {
    [Test]
    public void IsRectanglesIntersect_True_Test()
    {
      var rect1 = new Rectangle(1, 2, 3, 4);
      var rect2 = new Rectangle(1, 2, 3, 4);

      Assert.IsTrue(IntersectionCalculation.IsRectanglesIntersect(rect1, rect2));
    }

    [Test]
    public void IsRectanglesIntersect_False_Test()
    {
      var rect1 = new Rectangle(1, 2, 3, 4);
      var rect2 = new Rectangle(100, 2, 3, 4);

      Assert.IsFalse(IntersectionCalculation.IsRectanglesIntersect(rect1, rect2));
    }
  }
}