﻿using System;
using System.Collections.Generic;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class MaxProductOfIntegersTests
  {
    [Test]
    public void TestAllNegatives()
    {
      var input = new List<int> { -2, -3, -4, -5 };

      Assert.AreEqual(-2 * 3 * 4, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestNormal()
    {
      var input = new List<int> { 0, 1, 2, 3, 3, 4, 5 };
      Assert.AreEqual(5 * 4 * 3, MaxProductOfIntegers.MaxThreeProduct(input));
    }


    [Test]
    public void TestOneNegative()
    {
      var input = new List<int> { 3, 2, -1, -4, -5 };
      Assert.AreEqual(3 * 4 * 5, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestTwoNegative()
    {
      var input = new List<int> { 3, -3, -1, -4, -5 };
      Assert.AreEqual(3 * 4 * 5, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestZeroNegativesButNegativeProductBigger()
    {
      var input = new List<int> { 3, 3, 1, -4, -5 };
      Assert.AreEqual(3 * 4 * 5, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestZeroNegativesButPositiveProductBigger()
    {
      var input = new List<int> { 6, 10, 5, -4, -5 };
      Assert.AreEqual(6 * 10 * 5, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestOneZeroAndPositiveProductLessThenNegativesAndOnePositive()
    {
      var input = new List<int> { 6, 10, -1, -2, 0, 0 };
      Assert.AreEqual(10 * -1 * -2, MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestArgumentExceptionWhenIntMinValueOverflow()
    {
      var input = new List<int> { 6, int.MinValue, int.MinValue };

      Assert.Throws<ArgumentException>(() => MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestArgumentExceptionWhenIntOverflow()
    {
      var input = new List<int> { int.MaxValue, int.MaxValue, 0 };
      Assert.Throws<ArgumentException>(() => MaxProductOfIntegers.MaxThreeProduct(input));
    }

    [Test]
    public void TestArgumentExceptionWhenInputIsNull()
    {
      Assert.Throws<ArgumentException>(() => MaxProductOfIntegers.MaxThreeProduct(null));
    }

    [Test]
    public void TestArgumentExceptionWhenInputDoNotContainsEnoughtElements()
    {
      Assert.Throws<ArgumentException>(() => MaxProductOfIntegers.MaxThreeProduct(new[] { 1, 2 }));
    }
  }
}