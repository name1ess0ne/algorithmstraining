﻿using System;
using System.Collections.Generic;
using System.Linq;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class MazeSolverTests
    {
        [Test]
        public void SolveMazeTest1()
        {
            var node1 = new MazeNode { Value = 1 };
            var node5 = new MazeNode { Value = 5 };
            var node7 = new MazeNode { Value = 7 };

            var maze = new MazeNode
            {
                Value = 1,
                SubNodes = new List<MazeNode>
                {
                    new MazeNode { Value = 2, SubNodes = new List<MazeNode>
                        {
                            new MazeNode 
                            { Value = 4, SubNodes = new List<MazeNode> 
                                {
                                    new MazeNode { Value = 6, SubNodes = new List<MazeNode> { node7 }}
                                }
                            }
                        }
                    },
                    new MazeNode
                    {
                        Value = 3,
                        SubNodes = new List<MazeNode>
                        {
                            node5,
                            node7,
                            new MazeNode
                            {
                                Value = 8,
                                SubNodes = new List<MazeNode>
                                {
                                    new MazeNode
                                    {
                                        Value = 9,
                                        SubNodes = new List<MazeNode>
                                        {
                                            node7,
                                            node1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };

            List<MazeWay> ways = MazeSolver.SolveMaze(maze, 7).ToList();

            foreach (MazeWay w in ways)
            {
                Console.WriteLine(w.ToString());
            }

            Assert.AreEqual(3, ways.Count);

            //TODO: ways can be returned in other order, fix test for it
            CollectionAssert.AreEqual(new MazeWay { 1, 2, 4, 6, 7 }, ways[0]);
            CollectionAssert.AreEqual(new MazeWay { 1, 3, 7 }, ways[1]);
            CollectionAssert.AreEqual(new MazeWay { 1, 3, 8, 9, 7 }, ways[2]);
        }

        [Test]
        public void SolveMazeTest2()
        {
            var node1 = new MazeNode { Value = 1 };
            var node2 = new MazeNode { Value = 2 };

            node1.SubNodes = new List<MazeNode> { node2 };
            node2.SubNodes = new List<MazeNode> { node1 };

            List<MazeWay> ways = MazeSolver.SolveMaze(node1, 7).ToList();

            CollectionAssert.IsEmpty(ways);
        }

        [Test]
        public void SolveMazeTest3()
        {
            List<MazeWay> ways = MazeSolver.SolveMaze(null, 7).ToList();

            CollectionAssert.IsEmpty(ways);
        }

        [Test]
        public void SolveMazeTest4()
        {
            var node = new MazeNode { Value = 1 };
            List<MazeWay> ways = MazeSolver.SolveMaze(node, 7).ToList();

            CollectionAssert.IsEmpty(ways);
        }

        [Test]
        public void SolveMazeTest5()
        {
            var node = new MazeNode { Value = 7 };
            List<MazeWay> ways = MazeSolver.SolveMaze(node, 7).ToList();

            Assert.AreEqual(1, ways.Count);
            CollectionAssert.Contains(ways.First(), node.Value);
        }
    }
}