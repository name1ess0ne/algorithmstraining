﻿using System.Collections.Generic;
using System.Linq;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class NumbersTests
  {
    [Test]
    public void NumberToDigitsTest()
    {
      const int number = 12345;
      int[] digits = { 1, 2, 3, 4, 5 };

      IEnumerable<int> result = Numbers.NumberToDigits(number).ToList();

      CollectionAssert.AreEqual(digits, result);
    }

    [Test]
    public void NumberToDigitsReversOrderTest()
    {
      const int number = 125;
      int[] digits = { 5, 2, 1 };

      IEnumerable<int> result = Numbers.NumberToDigitsReversOrder(number);

      CollectionAssert.AreEqual(digits, result);
    }

    [TestCase(128, 0, ExpectedResult = 8)]
    [TestCase(125, 1, ExpectedResult = 2)]
    [TestCase(125, 2, ExpectedResult = 1)]
    [TestCase(125, 3, ExpectedResult = 0)]
    [TestCase(387893278, 5, ExpectedResult = 8)]
    public int GetDigitByRankTest(int number, int rank)
    {
      return Numbers.GetDigitByRank(number, rank);
    }
  }
}