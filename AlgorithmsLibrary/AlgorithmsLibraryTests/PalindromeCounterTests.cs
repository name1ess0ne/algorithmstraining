﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class PalindromeCounterTests
  {
    [TestCase("aba", ExpectedResult = 1)]
    [TestCase("abba", ExpectedResult = 2)]
    [TestCase("xabaz", ExpectedResult = 1)]
    [TestCase("hzabbaxcf", ExpectedResult = 2)]
    [TestCase("hzabbazcf", ExpectedResult = 3)]
    [TestCase("abbaaba", ExpectedResult = 5)]
    public int CountPalindromeTest(string str)
    {
      return PalindromeCounter.CountPalindrome(str);
    }
  }
}
