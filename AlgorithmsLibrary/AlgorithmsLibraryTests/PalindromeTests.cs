﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class PalindromeTests
    {
        [TestCase("Noel sees Leon.", ExpectedResult = true)]
        [TestCase("noel sees leon.", ExpectedResult = true)]
        [TestCase("noEl seXes leOn.", ExpectedResult = true)]
        [TestCase("noel sexs leon.", ExpectedResult = false)]
        [TestCase("nOEl ses lEOn.", ExpectedResult = true)]
        [TestCase("n", ExpectedResult = true)]
        [TestCase("", ExpectedResult = true)]
        public bool IsPalindromeTest(string str)
        {
            return Palindrome.IsPalindrome(str);
        }
    }
}