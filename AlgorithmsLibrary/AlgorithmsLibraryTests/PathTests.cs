﻿using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class PathTests
    {
        [TestCase("/a/b/c/d", "../x", ExpectedResult = "/a/b/c/x")]
        [TestCase("/a/b/c/d", "..", ExpectedResult = "/a/b/c")]
        [TestCase("/a/b/c/d", "/", ExpectedResult = "/")]
        [TestCase("/a/b/c/d", "/x", ExpectedResult = "/x")]
        [TestCase("/a/b/c/d", "../../../../../x", ExpectedResult = "/x")]
        [TestCase("/a/b/c/d", "../../c/../x", ExpectedResult = "/a/b/x")]
        public string CdTest(string currentPath, string newPath)
        {
            var path = new Path(currentPath);
            return path.Cd(newPath).CurrentPath;
        }
    }
}