﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class PhoneNumbersTests
    {
        [Test]
        public void ReformatTest()
        {
            string[] phones = { "1234567890", "123-456-7890" };

            PhoneNumbers.Reformat(phones);

            Assert.AreEqual("456-123-7890", phones[0]);
            Assert.AreEqual("456-123-7890", phones[1]);
        }

        [Test]
        public void Reformat2Test()
        {
            string[] phones = { "1234567890", "123-456-7890" };

            PhoneNumbers.Reformat2(phones);

            Assert.AreEqual("456-123-7890", phones[0]);
            Assert.AreEqual("456-123-7890", phones[1]);
        }
    }
}