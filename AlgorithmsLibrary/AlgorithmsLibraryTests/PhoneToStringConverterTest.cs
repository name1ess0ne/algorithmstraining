﻿using System.Collections.Generic;
using System.Linq;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class PhoneToStringConverterTest
  {
    [Test]
    public void PhoneToString()
    {
      var expected = new List<string>
      {
        "ad", "bd", "cd",
        "ae", "be", "ce",
        "af", "bf", "cf"
      };

      IEnumerable<string> result = PhoneToStringConverter.PhoneToString(12);

      CollectionAssert.AreEquivalent(expected, result);
    }

    [Test]
    public void PhoneToStringOneDigitNumber()
    {
      var expected = new List<string> {"a", "b", "c"};

      IEnumerable<string> result = PhoneToStringConverter.PhoneToString(1);

      CollectionAssert.AreEquivalent(expected, result);
    }

    [Test]
    public void PhoneToStringThreeDigits()
    {
      var expected = new List<string>
      {
        "adg", "bdg", "cdg",
        "aeg", "beg", "ceg",
        "afg", "bfg", "cfg",
        "adh", "bdh", "cdh",
        "aeh", "beh", "ceh",
        "afh", "bfh", "cfh",
        "adi", "bdi", "cdi",
        "aei", "bei", "cei",
        "afi", "bfi", "cfi"
      };

      IEnumerable<string> result = PhoneToStringConverter.PhoneToString(123);

      CollectionAssert.AreEquivalent(expected, result);
    }
  }
}