﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class RunTests
  {
    [TestCase("abbcccddddcccbba", ExpectedResult = 6)]
    [TestCase("abbbb", ExpectedResult = 1)]
    public int IndexOfLongestRunTest(string str)
    {
      return Run.IndexOfLongestRun(str);
    }

    [TestCase("abbcccddddcccbba", ExpectedResult = 6)]
    [TestCase("abbbb", ExpectedResult = 1)]
    public int IndexOfLongestRunTwoTest(string str)
    {
      return Run.IndexOfLongestRunTwo(str);
    }
  }
}