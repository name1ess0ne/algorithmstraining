﻿using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class TextContainsAllWordsTests
    {
        [Test]
        public void IsTextContainsAllWordsTest1()
        {
            string[] words = { "a" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("a", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest2()
        {
            string[] words = { "a", "b" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("ab", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest3()
        {
            string[] words = { "a", "c" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("ab", words);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest4()
        {
            string[] words = { "a", "ab" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("ab", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest5()
        {
            string[] words = { "a", "b", "bc" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("abbbc", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest6()
        {
            string[] words = { "a", "b", "ab" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("ab", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest7()
        {
            string[] words = { "a", "b", "d" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("abcd", words);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest8()
        {
            string[] words = { "ac", "acc", "cn" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("accnacccn", words);

            Assert.IsTrue(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest9()
        {
            string[] words = { "ac", "acc", "cn" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("", words);

            Assert.IsFalse(result);
        }

        [Test]
        public void IsTextContainsAllWordsTest10()
        {
            string[] words = { "ab", "a", "bc", "c" };
            bool result = TextContainsAllWords.IsTextContainsAllWords("abbc", words);

            Assert.IsTrue(result);
        }
    }
}
