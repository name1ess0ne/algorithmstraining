﻿using AlgorithmsLibrary;
using AlgorithmsLibrary.DataStructures;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
  [TestFixture]
  public class TreeTraversalTests
  {
    #region Private Fields

    private readonly Node _root = new Node
    {
      Left = new Node
      {
        Value = 2,
        Left = new Node {Value = 1},
        Right = new Node {Value = 3}
      },
      Value = 5,
      Right = new Node
      {
        Value = 7,
        Left = new Node {Value = 6},
        Right = new Node {Value = 8}
      }
    };

    #endregion

    #region Pre-Order

    [Test]
    public void PreOrderTest()
    {
      string result = string.Empty;

      TreeTraversal.PreOrder(_root, n => result += n.Value + ";");

      Assert.AreEqual("5;2;1;3;7;6;8;", result);
    }

    [Test]
    public void PreOrderEnumerableTest()
    {
      string result = string.Empty;

      foreach (Node node in TreeTraversal.PreOrder(_root))
      {
        result += node.Value + ";";
      }

      Assert.AreEqual("5;2;1;3;7;6;8;", result);
    }

    [Test]
    public void PreOrderIterativeTest()
    {
      string result = string.Empty;

      TreeTraversal.PreOrderIterative(_root, n => result += n.Value + ";");

      Assert.AreEqual("5;2;1;3;7;6;8;", result);
    }

    #endregion

    #region In-Order

    [Test]
    public void InOrderTest()
    {
      string result = string.Empty;

      TreeTraversal.InOrder(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;2;3;5;6;7;8;", result);
    }

    [Test]
    public void InOrderIterativeTest()
    {
      string result = string.Empty;

      TreeTraversal.InOrderIterative(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;2;3;5;6;7;8;", result);
    }

    [Test]
    public void InOrderIterativeWithCacheTest()
    {
      string result = string.Empty;

      TreeTraversal.InOrderIterativeWithCache(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;2;3;5;6;7;8;", result);
    }

    #endregion

    #region Post-Order

    [Test]
    public void PostOrderTest()
    {
      string result = string.Empty;

      TreeTraversal.PostOrder(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;3;2;6;8;7;5;", result);
    }

    [Test]
    public void PostOrderIterativeTest()
    {
      string result = string.Empty;

      TreeTraversal.PostOrderIterative(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;3;2;6;8;7;5;", result);
    }

    [Test]
    public void PostOrderIterativeTwoStacksTest()
    {
      string result = string.Empty;

      TreeTraversal.PostOrderIterativeTwoStacks(_root, n => result += n.Value + ";");

      Assert.AreEqual("1;3;2;6;8;7;5;", result);
    }
    #endregion

    #region Level-Order

    [Test]
    public void LevelOrderTest()
    {
      string result = string.Empty;

      TreeTraversal.LevelOrder(_root, n => result += n.Value + ";");

      Assert.AreEqual("5;2;7;1;3;6;8;", result);
    }

    #endregion
  }
}