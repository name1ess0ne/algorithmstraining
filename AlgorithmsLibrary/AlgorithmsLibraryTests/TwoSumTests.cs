﻿using System;
using System.Collections.Generic;
using AlgorithmsLibrary;
using NUnit.Framework;

namespace AlgorithmsLibraryTests
{
    [TestFixture]
    public class TwoSumTests
    {
        [Test]
        public void FindTwoSumTest()
        {
            Tuple<int, int> indices = TwoSum.FindTwoSum(new List<int> {1, 3, 5, 7, 9}, 12);
            Console.WriteLine(indices.Item1 + " " + indices.Item2);
        }
    }
}